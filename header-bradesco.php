<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Author" content="Agência Baked | baked.ag">

<title><?php wp_title( '|', true, 'right' ); ?> PRIMETOUR</title>

<!--FAVICON-->
<link rel="icon" type="image/ico" href="<?php bloginfo('template_url'); ?>/images/favicon.ico"/>

<!--CSS-->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<!--JAVASCRIPT-->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_ui.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/skel.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/skel_menu.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/banners.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scripts_bradesco.js"></script>
<?php wp_head(); ?>
</head>

<body <?php body_class( $class ); ?>>
<!--GOOGLE ANALYTICS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-76582201-1', 'auto');
  ga('send', 'pageview');
</script>

<!--MENU MOBILE-->
<?php get_sidebar('bradesco'); ?> 


<!--GERAL-->
<div class="geral">

  <!--CABEÇALHO-->
  <div id="cabecalho" class="bradesco">
    <div id="head_bradesco">
      <a href="<?php the_permalink('8396'); ?> "><img src="<?php bloginfo('template_url'); ?>/images/bradesco_cartoes.png" id="logo_bradesco"></a>
      <a href="<?php echo site_url(); ?>" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/prime_bradesco.png" id="prime_bradesco"></a>
    </div>
  </div>