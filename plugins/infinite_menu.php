<?php if ( is_user_logged_in() ) { ?>
    <div id="menu_infinite">
        <div id="nome">
            <?php 
                global $current_user;
                wp_get_current_user();
                echo 'Olá, ' . $current_user->display_name . "\n";
            ?>
        </div>
        <a href="<?php echo wp_logout_url( home_url() ); ?>" id="logout_infinite">Sair</a>
    </div>
<?php } ?>