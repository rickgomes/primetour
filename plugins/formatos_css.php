<?php
/*
Plugin Name: Custom Styles
Plugin URI: http://www.speckygeek.com
Description: Add custom styles in your posts and pages content using TinyMCE WYSIWYG editor. The plugin adds a Styles dropdown menu in the visual post editor.
Based on TinyMCE Kit plug-in for WordPress
http://plugins.svn.wordpress.org/tinymce-advanced/branches/tinymce-kit/tinymce-kit.php
*/
/**
 * Apply styles to the visual editor
 */ 
add_filter('mce_css', 'tuts_mcekit_editor_style');
function tuts_mcekit_editor_style($url) {
 
    if ( !empty($url) )
        $url .= ',';
 
    // Retrieves the plugin directory URL
    // Change the path here if using different directories
    $url .= trailingslashit( plugin_dir_url(__FILE__) ) . '/editor-styles.css';
 
    return $url;
}
 
/**
 * Add "Styles" drop-down
 */
add_filter( 'mce_buttons_2', 'tuts_mce_editor_buttons' );
 
function tuts_mce_editor_buttons( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
 
/**
 * Add styles/classes to the "Styles" drop-down
 */
add_filter( 'tiny_mce_before_init', 'tuts_mce_before_init' );
 
function tuts_mce_before_init( $settings ) {
 
    $style_formats = array(
        array(
            'title' => 'Título',
            'selector' => 'div',
            'classes' => 'titulo',
        ),
        array(
            'title' => 'Sub Título',
            'block' => 'div',
            'classes' => 'subtitulo',
        ),
        array(
            'title' => 'Marrom 30px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'texto_prime'
        ),
        array(
            'title' => 'Marrom Wide 30px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'texto_prime_wide'
        ),
        array(
            'title' => 'Marrom 24px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'texto_corporativo'
        ),
        array(
            'title' => 'Laranja Wide 48px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'titulo_contato'
        ),
        array(
            'title' => 'Branco Wide 36px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'titulo_branco'
        ),
        array(
            'title' => 'Branco Wide 24px',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'wide_branco_24'
        ),
        array(
            'title' => 'Padrão Dia',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'padrao_dia'
        ),
        array(
            'title' => 'Padrão Bradesco',
            'inline' => 'span',
            'selector' => 'p',
            'classes' => 'titulo_bradesco'
        ),
//        array(
//            'title' => 'Red Uppercase Text',
//            'inline' => 'span',
//            'styles' => array(
//                'color' => '#ff0000',
//                'fontWeight' => 'bold',
//                'textTransform' => 'uppercase'
//            )
//        )
    );
 
    $settings['style_formats'] = json_encode( $style_formats );
 
    return $settings;
 
}
 
/* Learn TinyMCE style format options at http://www.tinymce.com/wiki.php/Configuration:formats */
 
/*
 * Add custom stylesheet to the website front-end with hook 'wp_enqueue_scripts'
 */
add_action('wp_enqueue_scripts', 'tuts_mcekit_editor_enqueue');
 
/*
 * Enqueue stylesheet, if it exists.
 */
function tuts_mcekit_editor_enqueue() {
  $StyleUrl = get_stylesheet_directory_uri() .'/css/padroes.css'; // Customstyle.css is relative to the current file
  wp_enqueue_style( 'myCustomStyles', $StyleUrl );
}
?>