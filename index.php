<?php get_header(); ?>

<!--BANNER HOME-->
<div id="banner_home" class="cycle-slideshow"
data-cycle-fx="fade"
data-cycle-swipe=true
data-cycle-pause-on-hover="false"
data-cycle-speed="250"
data-cycle-slides="> div.item">

	<!--PÁGINAÇÃO DO BANNER-->
    <div class="cycle-pager"></div>
    
	<!--CABEÇALHO-->
    <div id="cabecalho">
        <!--MENU-->
    	<nav id="menu">
        	<a href="<?php echo site_url(); ?>" id="logo"></a>
			<?php $opcoes_menu = array('menu'=> 'menu','container'=> '', 'theme_location'  => 'menu','items_wrap'=> '<ul class="menu_superior">%3$s</ul>',); wp_nav_menu( $opcoes_menu ); ?>
        </nav>
        
    	<nav id="sub-menu">
			<?php $menu_especiais = array('menu'=> 'submenu_especiais','container'=> '', 'theme_location'  => 'submenu_especiais','items_wrap'=> '<ul id="sub-especiais">%3$s</ul>',); wp_nav_menu( $menu_especiais ); ?>
			<?php $menu_destinos = array('menu'=> 'submenu_destinos','before'=> '<span class="escolha">Escolha o seu</span>', 'container'=> '', 'theme_location'  => 'submenu_destinos','items_wrap'=> '<ul id="sub-destinos">%3$s</ul>',); wp_nav_menu( $menu_destinos ); ?>
        </nav>
    </div>
    
    <?php query_posts(array('posts_per_page'=> '6', 'post_type' => 'banner', 'banners' => 'principal')); ?>
    <?php if (have_posts()): while (have_posts()) : the_post(); ?> 
    <?php $titulo_banner = rwmb_meta('prime__titulo_banner_principal'); ?>
    <?php $subtitulo_banner = rwmb_meta('prime__subtitulo_banner_principal'); ?>
    <?php $link_banner = rwmb_meta('prime__link_banner'); ?>
    <?php $banner_mobile = rwmb_meta( 'prime__banner_mobile', 'type=image&size=banner_mobile' ); ?>
	<div class="item">
    	<?php the_post_thumbnail('banner_principal', array( 'class' => 'desktop' )); ?>
        <?php foreach ( $banner_mobile as $image ) { echo "<img src='{$image['url']}' alt='{$image['alt']}' class='mobile' /> "; } ?>
    	<div class="box">
            <p class="titulo"><?php echo $titulo_banner; ?></p>
            <p class="subtitulo"><?php echo $subtitulo_banner; ?></p>
            <a href="<?php echo $link_banner; ?>" class="bt_saiba">SAIBA MAIS</a>
        </div>
    </div>
   <?php endwhile; endif; ?> 
</div>

<!--CHAMADA-->
<div class="box_chamada">
    <p class="titulo_chamada">Qual sua próxima viagem?</p>
    <p class="sub_chamada">DESTINOS INCRÍVEIS, EXPERIÊNCIAS EXCLUSIVAS</p>
</div>

<!--DESTAQUES HOME-->
<div id="destaques_home">
    <?php query_posts(array('posts_per_page'=> '4', 'post_type' => 'banner', 'banners' => 'destaques')); ?>
    <?php if (have_posts()): while (have_posts()) : the_post(); ?> 
    <?php $titulo_destaque = rwmb_meta('prime__titulo_destaque_homel'); ?>
    <?php $texto_destaque = rwmb_meta('prime__texto_destaque_homel'); ?>
    <?php $link_banner = rwmb_meta('prime__link_banner'); ?>
    <?php $imagem_destaque = rwmb_meta( 'prime__banner_mobile', 'type=image&size=destaque_home_mobile' ); ?>
	<a href="<?php echo $link_banner; ?>" class="box">
    	<?php the_post_thumbnail('destaque_home', array( 'class' => 'grande' )); ?>
        <?php foreach ( $imagem_destaque as $image ) { echo "<img src='{$image['url']}' alt='{$image['alt']}' class='mobile' /> "; } ?>
        <div class="fade_black"></div>
    	<div class="caixa_texto">
        	<div class="texto">
            	<span class="titulo"><?php echo $titulo_destaque; ?></span>
                <p><?php echo $texto_destaque; ?></p>
            </div>
        </div>
    </a>
   <?php endwhile; endif; ?> 
    <div class="clear"></div>
</div>

<!--CHAMADA-->
<div class="box_chamada">
    <p class="titulo_chamada">Instagram</p>
</div>

<!--FEED INSTAGRAM-->
<div id="feed_instagram">
	<?php echo do_shortcode('[instagram-feed]'); ?>
</div>

<!--NEWSLETTER-->
<div id="box_newsletter">
    <!--CHAMADA-->
    <div class="box_chamada">
        <p class="titulo_chamada">Newsletter</p>
        <p class="sub_chamada">Aventure-se nas novidades que vamos te mandar por e-mail.</p>
    </div>
    <?php echo do_shortcode( '[contact-form-7 id="6" title="Newsletter"]' ); ?>
</div>

<?php get_footer(); ?>