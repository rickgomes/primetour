<?php get_header(); ?>

<?php 
//OBTENDO HOTÉIS
global $post; 
$term_id = get_the_terms($post->id, 'destinos');
$get_tags = get_the_terms($post->id, 'tags_hotel');
?>	 

<!--CONTEÚDO-->
<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
           	<div class="subtitulo"><?php echo rwmb_meta('prime_titulo_hotel'); ?></div>
           	<div class="titulo"><?php the_title(); ?></div>
            
            <?php $images = rwmb_meta( 'prime_slider_fotos', 'type=image&size=interna_padrao' );
			if ($images =="") {
			} else {
				echo '<div class="banner_principal">';
				echo '<div id="slider_destino" class="cycle-slideshow"
					data-cycle-fx="fade"
					data-cycle-swipe=true
					data-cycle-pause-on-hover="false"
					data-cycle-speed="400"
					data-cycle-slides="> img">';
            	foreach ( $images as $image ) { echo "<img src='{$image['url']}' alt='{$image['alt']}' /> "; }
				echo '<div id="paginacao_destino" class="cycle-pager"></div></div></div>'; 
			}
			?>
            
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
            <!--COLUNA ESQUERDA-->
            <div id="infos_destino">
            
            	<?php the_content(); ?>
                
                <!--OBTENDO AS TAGS DOS HOTÉIS-->
                
				<?php if(empty($get_tags)) { ?>
                <?php } else { ?>
                    <div id="box_especial">
                    <p class="padrao_dia">Por que é especial:</p>
                <?php foreach( $get_tags as $tag ): setup_postdata($tag); ?>
                       <li><?php echo $tag->name; ?></li>
                <?php endforeach; ?>
                    <div class="clear"></div></div>
                <?php } ?>			
                
                
                <!--EXPERIÊNCIAS-->
                <?php if ((rwmb_meta('prime_experiencia')) == ""){ ?>
				<?php } else { ?>
                    <span class="titulo_experiencia">Experiences</span>
                    <div id="box_experiencia">
                        <?php echo rwmb_meta('prime_experiencia'); ?>
                    </div>	
				<?php } ?>
                
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="quero_ir">Quero IR!</a>
            </div>
            
            <!--COLUNA DIREITA-->
            <div id="sidebar_destino">
            
				<?php $link_mapa = rwmb_meta('prime_mapa');
                if ($link_mapa == "") {
                } else {
                echo '<div id="tempo">';
                echo do_shortcode ('[awesome-weather location="'.$link_mapa.'" override_title="'.$link_mapa.'" units="C" size="wide" forecast_days="3" hide_stats="0" custom_bg_color="#e9e9e9" inline_style="width: 100%;" background_by_weather="0" text_color="#897f7f" locale="en"]');
                echo '</div>';
                }
                 ?>
                
                <div id="meses">
                	<span id="titulo_mes">melhores meses para viajar</span>
                <?php 
					$walker = new My_Walker_Category();
					$args = array( 'walker' => $walker ,'hide_empty'=> 0,'current_category'=> 1,'taxonomy'=> 'mes','orderby'=> 'id','order'=> 'ASC','title_li'=>'',);
					wp_list_categories($args);
				?>
                </div>
                
                    	<?php
						$endereco = rwmb_meta('prime_endereco');
						$telefones = rwmb_meta('prime_telefones');
						$site = rwmb_meta('prime_site');
						$opinioes = rwmb_meta('prime_opinioes'); 
						$latitude = rwmb_meta('prime_latitude'); 
						$longitude = rwmb_meta('prime_longitude'); 
						?>
                        
						<?php if ($latitude == "" && $longitude == "") { ?>
							<?php if ($endereco == "") { ?>
                            <?php } else { ?>
                            <div id="mapa_google">
                                <span id="titulo_mapa">Localização</span>
                                <?php echo do_shortcode ('[flexiblemap address="'.$endereco.'" directions="false" width="100%" height="250px" hidemaptype="true" hidescale="false" hidezooming="true" locale="pt-BR"]'); ?>
                                <div id="box_infos">
                                    <p><?php echo $endereco , $telefones; ?></p>
                                </div>
                            </div>
                            <?php } ?>
						<?php } else { ?>
                            <div id="mapa_google">
                                <span id="titulo_mapa">Localização</span>
                                <?php echo do_shortcode ('[flexiblemap center="'.$latitude.','.$longitude.'" directions="false" title="'.$endereco.'" width="100%" height="250px" hidemaptype="true" hidescale="false" hidezooming="true" locale="pt-BR"]'); ?>
                                <div id="box_infos">
                                    <p><?php echo $endereco , $telefones; ?></p>
                                </div>
                            </div>
						<?php } ?>
                <?php if ($site == "") { } else { echo '<a href="'.$site.'" target="_blank" class="fale_conosco hotel">Visite o site</a>'; }; ?>
                <?php if ($opinioes == "") { } else { echo '<a href="'.$opinioes.'" target="_blank" class="opinioes">Opiniões de Viajantes</a>'; }; ?>
                
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="fale_conosco hotel">Fale Conosco</a>
            </div>
            
            <div class="clear"></div>
                
            <?php endwhile; endif; ?>
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>