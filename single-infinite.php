<?php include (TEMPLATEPATH . '/plugins/infinite_controle.php'); ?>

<?php get_header(); ?>

<?php 
global $post; 
$get_tags = get_the_terms($post->id, 'tags_infinite');
?>

<?php $roteiros = $_GET['roteiros']; ?>


<!--CONTEÚDO-->
<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
           	<div class="subtitulo">
                <?php if(empty($roteiros)) { ?> 
                   PRONTA REFERÊNCIA
                <?php } else { ?>
                    <?php the_title(); ?>
                <?php } ?>
            </div>
           	<div class="titulo">
               <?php if(empty($roteiros)) { ?> 
                    <?php the_title(); ?>
                <?php } else { ?>
                    ROTEIROS
                <?php } ?>
                   
            </div>
            
            <?php $images = get_field('imagem_principal'); ?>
			<?php if (!empty($images)) { ?>
                <div class="banner_principal">
                    <img src="<?php echo $images; ?>" alt="<?php the_title(); ?>" />
                </div>
			<?php } ?>
            
            <?php // SE NÃO TIVER VARIAVEL DE ROTEIROS NA URL ?>
            <?php if(empty($roteiros)) { ?> 
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <!--COLUNA ESQUERDA-->
                    <div id="infos_destino">
                        <?php the_content(); ?>
                        
                        <!--OBTENDO AS TAGS-->
                        <?php if(empty($get_tags)) { ?>
                        <?php } else { ?>
                            <div id="box_especial">
                            <p class="padrao_dia">Acesse o arquivo e encontre:</p>
                        <?php foreach( $get_tags as $tag ): setup_postdata($tag); ?>
                            <li><?php echo $tag->name; ?></li>
                        <?php endforeach; ?>
                            <div class="clear"></div></div>
                        <?php } ?>			
                    </div>
                    
                    <!--COLUNA DIREITA-->
                    <div id="sidebar_destino">
                        <div id="referencias">
                            <span id="titulo">acesse a pronta referência</span>
                            <?php $link_html = get_field('link_html'); ?>
                            <?php $link_pdf = get_field('link_pdf'); ?>
                            <?php if (!empty($link_html)){ ?>
                                <a href="<?php echo $link_html; ?>" target="_blank" id="bt_roteiros" class="bt_referencia">Arquivo em HTML</a>
                            <?php } ?>
                            <?php if (!empty($link_pdf)){ ?>
                                <a href="<?php echo $link_pdf; ?>" target="_blank" id="bt_roteiros" class="bt_referencia">Arquivo em PDF</a>
                            <?php } ?>
                            <a href="?roteiros=1" id="bt_roteiros" class="bt_referencia">Roteiros Exclusivos</a>
                        </div>

                        <div id="meses">
                            <span id="titulo_mes">melhores meses para viajar</span>
                            <?php 
                                $walker = new My_Walker_Category();
                                $args = array( 'walker' => $walker ,'hide_empty'=> 0,'current_category'=> 1,'taxonomy'=> 'mes','orderby'=> 'id','order'=> 'ASC','title_li'=>'',);
                                wp_list_categories($args);
                            ?>
                        </div>
                    </div>
                    
                    <div class="clear"></div>
                <?php endwhile; endif; ?>
            <?php // SE TIVER ?>
            <?php } else { ?>
                <div id="box_resultados" class="hotel">
                    <div id="box_destino">
                        <?php $post_roteiro = get_posts(array(
                            'post_type' => 'infinite_roteiros',
                            'posts_per_page'   => '-1',
                            'orderby' => 'name', 
                            'order' => 'ASC',
							'meta_query' => array(
								array(
									'key' => 'infinite_pais',
									'value' => '"' . get_the_ID() . '"',
									'compare' => 'LIKE'
								)
							)
						));
						?>
                        <?php if (!empty($post_roteiro)){ ?>
                        <?php foreach( $post_roteiro as $post ): ?>
                            <a href="<?php echo get_permalink( $post->ID ); ?>" class="box_post">
                                <?php echo get_the_post_thumbnail($post->ID, 'resultado_busca', array('class' => 'imagem_post')); ?>
                                <div class="fade_black"></div>
                                <div class="fade_mais"></div>
                                <div class="infos">
                                    <span class="nome_cat"><?php echo $post->post_title; ?></span>
                                    <span class="titulo_post"><?php the_field('titulo_thumb', $post->ID); ?></span>
                                    <p class="descricao"><?php the_field('resumo_thumb', $post->ID); ?></p>
                                </div>
                            </a>
                        <?php endforeach; ?>
                        <?php } else { ?>
                            <span>Não possui roteiros cadastrados</span>
                        <? } ?>
                        <div class="clear"></div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>