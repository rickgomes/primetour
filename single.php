<?php get_header(); ?>

<!--CONTEÚDO-->

<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    	
        <!--CONTEÚDO DO POST-->
    	<div class="box_texto">
                <div class="categoria_blog">
                    <?php $nome_categoria = get_the_category($post->ID); ?>
                    <?php if(($nome_categoria[0]->term_id) == 1) { echo $nome_categoria[1]->cat_name; } else { echo $nome_categoria[0]->cat_name; } ?>
                </div>
                <div class="titulo_blog"><?php the_title(); ?></div>
                <span class="data">Publicado em <?php the_time('j') ?> de <?php the_time('F') ?> de <?php the_time('Y') ?></span>
            
                <?php if ( has_post_thumbnail() ) { 
                      echo '<div class="banner_principal" style="margin-bottom:50px;">';
                      the_post_thumbnail('interna_padrao'); 
                      echo '</div>';
                }; ?>
                
            <div id="conteudo_blog_interna" class="box_texto texto_especiais">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
               
                    <?php the_content(); ?>
                    
                    <!--<span class="autor_nome"><?php //the_author_firstname(); ?> <?php //the_author_lastname(); ?></span>
                    <span class="autor_funcao">Consultor Primetour</span>-->
                    
                    <a class="single_post" href="<?php echo site_url(); ?>/contato/" id="bt_contato" title="Fale Conosco" onclick="Post <?php the_title();?>">
                        <span>Fale Conosco</span>
                    </a>
                    
                    <!--REDES SOCIAIS-->
                    <div id="share">
                    	<span class="titulo">compartilhe esse post:</span>
                        <ul>
                        	<li><a href="http://www.facebook.com/share.php?u=<?php echo current_page_url(); ?>&t=<?php the_title();?>" class="facebook" target="_blank"></a></li>
                        	<li><a href="http://twitter.com/share?text=<?php the_title();?>&url=<?php echo current_page_url(); ?>&counturl=<?php echo current_page_url(); ?>" class="twitter" target="_blank"></a></li>
                        	<li><a href="https://plus.google.com/share?url=<?php echo current_page_url(); ?>" target="_blank" class="google"></a></li>
                        </ul>
                    </div>
                    
				<?php endwhile; endif; ?>
            </div>
        </div>
            
        <!--POSTS RELACIONADOS-->
        <?php $nodup[]=$post->ID; ?>
		<?php query_posts(array('posts_per_page'=> '3', 'post_type' => 'post', 'cat'=> ''.$nome_categoria[1]->term_id.'', 'post__not_in'=> $nodup)); ?>
        <?php if (have_posts()) : ?>
            <div id="box_relacionados">
                <span class="titulo">Posts Relacionados</span>
                <div id="box_blog">
                    <div id="box_resultados" class="posts_blog" style="padding:0; border:none; margin:0;">
						<?php while (have_posts()) : the_post(); ?>
                            <?php get_template_part('content', 'post_blog'); ?>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        
    </div>
</div>

<?php get_footer(); ?>