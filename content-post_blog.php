<a href="<?php the_permalink(); ?>" class="box_post">

	<?php $nome_categoria = get_the_category($post->ID); ?>
    
	<?php echo the_post_thumbnail('resultado_busca', array( 'class' => 'imagem_post' ) ); ?>
    <div class="fade_black"></div>
    <div class="fade_mais"></div>
    <div class="infos">
    	<span class="nome_cat"><?php if(($nome_categoria[0]->term_id) == 1) { echo $nome_categoria[1]->cat_name; } else { echo $nome_categoria[0]->cat_name; } ?></span>
		<span class="titulo_post"><?php the_title(); ?></span>
    </div>
</a>
