<?php if ( is_404() ) { ?>
<?php } else { ?>

<!--RODAPÉ-->
<div id="rodape">
	<ul id="redes_sociais">
    	<li><a href="http://www.facebook.com/primetourofficialpage" target="_blank" class="facebook"></a></li>
    	<li><a href="http://www.instagram.com/primetourviagens" target="_blank" class="instagram"></a></li>
        <li><a href="http://www.linkedin.com/company/primetour-ag%C3%AAncia-de-viagens" target="_blank" class="linkedin"></a></li>
        <li><a href="http://youtube.com/primetour" target="_blank" class="youtube"></a></li>
    </ul>
    
    <nav id="menu">
	<?php $opcoes_menu = array('menu'=> 'menu_rodape','container'=> '', 'theme_location'  => 'menu_rodape','items_wrap'=> '<ul>%3$s</ul>',); wp_nav_menu( $opcoes_menu ); ?>
    </nav>
    
    <a href="http://www.virtuoso.com" target="_blank" id="logo_virtuoso"></a>
    
    <p class="direitos">Copyright © <?php echo date('Y'); ?> primetour viagens. All rights reserved.</p>
    
    <a href="http://www.baked.ag" target="_blank" id="logo_baked"></a>
</div>
</div>
<?php } ?>
<?php wp_footer(); ?>

<!-- INFINITE LOGIN CONTROLE -->
<?php include (TEMPLATEPATH . '/plugins/infinite_login.php'); ?>

</body>
</html>