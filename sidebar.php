<div id="lg_mobile">
    <a href="<?php echo site_url(); ?>" id="logo_mobile"></a></a>
</div>

<div id="sidebar">
        <!--MENU-->
    	<nav id="menu">
        	<a href="<?php echo site_url(); ?>" id="logo"></a>
			<?php $opcoes_menu = array('menu'=> 'menu','container'=> '', 'theme_location'  => 'menu','items_wrap'=> '<ul class="menu_superior">%3$s</ul>',); wp_nav_menu( $opcoes_menu ); ?>
        </nav>
        
    	<nav id="sub-menu">
			<?php $menu_especiais = array('menu'=> 'submenu_especiais','container'=> '', 'theme_location'  => 'submenu_especiais','items_wrap'=> '<ul id="sub-especiais">%3$s</ul>',); wp_nav_menu( $menu_especiais ); ?>
			<?php $menu_destinos = array('menu'=> 'submenu_destinos','before'=> '<span class="escolha">Escolha o seu</span>', 'container'=> '', 'theme_location'  => 'submenu_destinos','items_wrap'=> '<ul id="sub-destinos">%3$s</ul>',); wp_nav_menu( $menu_destinos ); ?>
        </nav>
    </div>
</div>