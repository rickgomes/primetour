<?php if (is_page('infinite')) { ?>
    <?php $url_logado_redirect = site_url().'/infinite-destinos'; ?>
    <?php if ( is_user_logged_in() ) { ?>
        <?php if (is_page('infinite')) { ?>
            <?php $login = $_GET['login']; ?>
            <?php if ($login == "s") { ?>
                <?php wp_redirect($url_logado_redirect); ?>
                <?php exit; ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>
<?php } ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<?php $bg_pagina = rwmb_meta('prime_estilo'); ?>

<div id="conteudo_geral" class="<?php if ($bg_pagina) { echo $bg_pagina; } else { echo 'mapa'; }; ?>">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
			<?php the_content(); ?>
                
        <?php endwhile; endif; ?>
        
    </div>
    
</div>

<?php get_footer(); ?>