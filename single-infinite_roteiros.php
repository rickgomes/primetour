<?php include (TEMPLATEPATH . '/plugins/infinite_controle.php'); ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">

            <?php $infinite_relacionado = get_field('infinite_pais'); ?>
           	<div class="subtitulo"><?php echo $infinite_relacionado[0]->post_title; ?></div>
           	<div class="titulo"><?php the_title(); ?></div>
            
            <?php $images = get_field('imagem_slider');
			if ($images =="") {
			} else {
				echo '<div class="banner_principal">';
				echo '<div id="slider_destino" class="cycle-slideshow"
					data-cycle-fx="fade"
					data-cycle-swipe=true
					data-cycle-pause-on-hover="false"
					data-cycle-speed="400"
					data-cycle-slides="> img">';
            	foreach ( $images as $image ) { echo "<img src='{$image['url']}' alt='{$image['caption']}' /> "; }
				echo '<div id="paginacao_destino" class="cycle-pager"></div></div></div>'; 
			}
			?>
            
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
            <!--COLUNA ESQUERDA-->
            <div id="infos_destino">
            
            	<?php the_content(); ?>
                
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="quero_ir">Quero IR!</a>
            </div>
            
            <!--COLUNA DIREITA-->
            <div id="sidebar_destino">
            
				<?php $link_mapa = rwmb_meta('prime_mapa');
                if ($link_mapa == "") {
                } else {
                echo '<div id="tempo">';
                echo do_shortcode ('[awesome-weather location="'.$link_mapa.'" override_title="'.$link_mapa.'" units="C" size="wide" forecast_days="3" hide_stats="0" custom_bg_color="#e9e9e9" inline_style="width: 100%;" background_by_weather="0" text_color="#897f7f" locale="en"]');
                echo '</div>';
                }
                 ?>
                
                <div id="meses">
                	<span id="titulo_mes">melhores meses para viajar</span>
                <?php 
					$walker = new My_Walker_Category();
					$args = array( 'walker' => $walker ,'hide_empty'=> 0,'current_category'=> 1,'taxonomy'=> 'mes','orderby'=> 'id','order'=> 'ASC','title_li'=>'',);
					wp_list_categories($args);
				?>
                </div>
                
                <?php if ($site == "") { } else { echo '<a href="'.$site.'" target="_blank" class="fale_conosco hotel">Visite o site</a>'; }; ?>
                <?php if ($opinioes == "") { } else { echo '<a href="'.$opinioes.'" target="_blank" class="opinioes">Opiniões de Viajantes</a>'; }; ?>
                
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="fale_conosco hotel">Fale Conosco</a>
                <?php echo do_shortcode( '[wp_objects_pdf]' ); ?>
            </div>
            
            <div class="clear"></div>
                
            <?php endwhile; endif; ?>
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>