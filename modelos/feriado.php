<?php  /* Template Name: Feriado */ ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<?php $bg_pagina = rwmb_meta('prime_estilo'); ?>

<div id="conteudo_geral" class="mandala">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
            <div class="titulo">O mundo te espera.</div>
            <div class="subtitulo">Escolha o seu feriado.</div>
            
            <div id="apresentacao_destinos">
				<?php $terms = get_terms('feriado', array('hide_empty'=> 0,'parent'=> 0, 'orderby'=> 'id', 'order'=>'ASC'));
                if (!empty ($terms) && ! is_wp_error($terms)) {
                    foreach ($terms as $term) { ?>
                    
                    <a href=" <?php echo esc_url(get_term_link($term)) ?>" class="destino_box_gd">
                        <?php echo taxonomy_featured_image($term->term_id, 'filtro_imagem'); ?>
                        <div class="fade_black"></div>
                        <div class="legenda"><p><?php echo $term->name; ?></p><div class="linha"/></div></div>
                    </a>
                <?php }} ?>
            </div>
            
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>