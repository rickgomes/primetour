<?php  /* Template Name: Descubra - Resultados */ ?>

<?php get_header(); ?>

<?php  $tag_estilo = $_GET['estilo0']; ?>
<?php  $tag_estilo1 = $_GET['estilo1']; ?>
<?php  $tag_estilo2 = $_GET['estilo2']; ?>

<div id="conteudo_geral" class="mapa">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
            
                <div class="box_texto">
                
                    <div class="titulo">O QUE FAZ BRILHAR SEUS OLHOS?</div>
                    
                    <div class="subtitulo">Veja abaixo os resultados</div>
                    
                
                <? //Loop dos Hoteis e Destinos ?> 
                    
<?php 
$my_query = new WP_Query(array(
	'posts_per_page' => 3,
	'orderby'=> 'title',
	'order' => 'ASC',
	'paged' => $paged,
	'post_type' => 'hotel',
	'tax_query' => array(
		'relation' => 'OR',
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo1,
			),
		),
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo2,
			),
		),
	),
));

$do_not_duplicate = array(); // set befor loop variable as array

// 1. Loop
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'paged' => $paged,
	'post_type' => 'hotel',
	'tax_query' => array(
		'relation' => 'OR',
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo1,
			),
		),
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo2,
			),
		),
	),
));
while ( have_posts() ) : the_post();
	$locations = get_field('selecionar_destinos');
    $do_not_duplicate[] = $locations[0]->ID; // remember ID's in loop
endwhile;
	

// 2. Loop
query_posts(array(
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'paged' => $paged,
	'post_type' => 'hotel',
	'tax_query' => array(
		'relation' => 'OR',
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo1,
			),
		),
		array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo,
			),
			array(
				'taxonomy' => 'estilo',
				'field'    => 'slug',
				'terms'    =>  $tag_estilo2,
			),
		),
	),
));


while (have_posts()) : the_post();
    if ( !in_array( $post->ID, $do_not_duplicate ) ) { // check IDs         
	   $result = array_unique($do_not_duplicate);
    }
endwhile;


//var_dump ($result);
if (($result)=='') { 
	echo '<div id="resultado_busca" style="display:block; height:30px; margin:100px 0; text-align:center;">Não foram encontrados resultados.</div>';
} else {

	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	query_posts( array(
		'posts_per_page' => 3,
		'post_type' => 'destino',
		'orderby'=> 'title', 
		'order' => 'ASC',
		'paged' => $paged,
		'post__in'	=> $result
		));
	
	  if (have_posts()) :
		  echo '<div id="box_resultados" class="destino">';
		  echo '<div class="titulo_busca">Resultados para <span>Destinos</span></div>';
		  echo '<div id="box_destino">';
		  
	  while ( have_posts() ) : the_post();
		  get_template_part('content', 'destino');
	  endwhile;
	  
			echo wp_pagenavi();
			echo '</div>';
			echo '</div>';
			
	  else:		
	   echo 'Não foi possivel encontrar nenhum resultado.';
	  endif; 
	   wp_reset_query();
}
?>



				<?php 
				
				
                // PEGAR HOTÉIS
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$args =query_posts(array(
					'posts_per_page' => 3,
					'orderby'=> 'title',
					'order' => 'ASC',
					'paged' => $paged,
					'post_type' => 'hotel',
					'tax_query' => array(
					'relation' => 'OR',
					array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'estilo',
							'field'    => 'slug',
							'terms'    =>  $tag_estilo,
						),
						array(
							'taxonomy' => 'estilo',
							'field'    => 'slug',
							'terms'    =>  $tag_estilo1,
						),
					),
					array(
						'relation' => 'AND',
						array(
							'taxonomy' => 'estilo',
							'field'    => 'slug',
							'terms'    =>  $tag_estilo,
						),
						array(
							'taxonomy' => 'estilo',
							'field'    => 'slug',
							'terms'    =>  $tag_estilo2,
						),
					),
					),
				));
				//var_dump ($args);
                        
                        if( have_posts() ){
                            
                                echo '<div id="box_resultados" class="hotel">';
                                echo '<div class="titulo_busca">Resultados para <span>Hotéis</span></div>';
                                echo '<div id="box_destino">';
                            while( have_posts() ){
                                the_post();
                                
                                    get_template_part('content', 'hotel');
                                wp_reset_postdata();
                            }
                                echo wp_pagenavi();
                                echo '</div>';
                                echo '</div>';
                        }		
						wp_reset_query();		
                ?>
                </div> 
            </div>
        </div>        

<?php get_footer(); ?>