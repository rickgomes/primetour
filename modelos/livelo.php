<?php  /* Template Name: Livelo */ ?>

<?php get_header('livelo'); ?>

<!--CONTEÚDO-->
<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
			<?php the_content(); ?>
                
        <?php endwhile; endif; ?>
        
    </div>
    
</div>

<?php get_footer('livelo'); ?>