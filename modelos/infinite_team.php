<?php  /* Template Name: Team */ ?>

<?php include (TEMPLATEPATH . '/plugins/infinite_controle.php'); ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<?php $bg_pagina = rwmb_meta('prime_estilo'); ?>

<div id="conteudo_geral" class="mandala">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo" class="team">
    
    	<div class="box_texto">
        
            <div class="titulo">NOSSOS IC’s PARCEIROS</div>
            <div class="subtitulo">CONHEÇA OS CONSULTORES INDEPENDENTES QUE JÁ FAZEM PARTE DO NOSSO PROGRAMA</div>
            
            <div id="box_resultados" class="team" style="border-bottom: none;">

              <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
              <?php query_posts(array('posts_per_page'=> '-1', 'paged' => $paged, 'orderby' => 'agencias', 'order' => 'DESC', 'post_type' => 'infinite_team')); ?>

                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                  <?php global $post; $term = get_the_terms($post->id, 'agencias'); ?>

                  <a href="<?php the_permalink(); ?>" class="box_post">
                    <?php echo the_post_thumbnail('resultado_busca', array( 'class' => 'imagem_post' ) ); ?>
                    <div class="fade_black"></div>
                    <div class="fade_mais"></div>
                    <div class="infos">
                      <span class="nome_cat"><?php echo $term[0]->name; ?></span>
                      <span class="titulo_post"><?php the_title(); ?></span>
                      <p class="regiao"><?php the_field('regiao'); ?></p>
                      <p class="especialista">
                        <strong>Especialista em:</strong><br>
                        <?php the_field('especialista'); ?>
                      </p>
                    </div>
                  </a>

                <?php endwhile; endif; ?>
            </div>

            <div class="box_cta_infinite">
                <a href="http://primetour.com.br/infinite-contato/" class="ow-icon-placement-left ow-button-hover" id="bt_contato">
                    <span>Seja um IC</span>
                </a>
            </div>

        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>