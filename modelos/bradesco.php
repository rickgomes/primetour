<?php  /* Template Name: Bradesco */ ?>

<?php get_header('bradesco'); ?>

<!--CONTEÚDO-->
<div id="conteudo_geral" class="bradesco">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb" class="bradesco"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo" class="bradesco">
    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
			<?php the_content(); ?>
                
        <?php endwhile; endif; ?>
        
    </div>
    
</div>

<?php get_footer('bradesco'); ?>