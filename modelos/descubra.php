<?php  /* Template Name: Descubra */ ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<?php $bg_pagina = rwmb_meta('prime_estilo'); ?>

<div id="conteudo_geral" class="mandala">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
            <div class="titulo">O QUE FAZ BRILHAR SEUS OLHOS?</div>
            <div class="subtitulo">Queremos te inspirar e realizar seus sonhos!<br/> Escolha as 03 fotos que melhor traduzem seus desejos e a gente <br/>diz quais viagens mais combinam com seu momento.</div>
            
            <div id="apresentacao_destinos" class="box_descubra">
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_01.jpg"/>
                    <div class="selecao">gastronomia</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_02.jpg"/>
                    <div class="selecao">familia</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_03.jpg"/>
                    <div class="selecao">wellness_e_spa</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_04.jpg"/>
                    <div class="selecao">aventura</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_05.jpg"/>
                    <div class="selecao">romance</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_06.jpg"/>
                    <div class="selecao">praias</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_07.jpg"/>
                    <div class="selecao">safari</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_08.jpg"/>
                    <div class="selecao">lua_de_mel</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_09.jpg"/>
                    <div class="selecao">navios_cruzeiros_e_yatchs</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_10.jpg"/>
                    <div class="selecao">ski</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_11.jpg"/>
                    <div class="selecao">esportes</div>
                    <div class="fade_black"></div>
                </div>
                <div class="destino_box_pq cx_descubra">
                	<img src="<?php bloginfo('template_url'); ?>/images/descubra/descubra_12.jpg"/>
                    <div class="selecao">city_break</div>
                    <div class="fade_black"></div>
                </div>
                
                <!--BOTÃO DESCUBRA-->
                <div class="clear"></div>
                <div id="resultado_descubra"></div>
                <div id="bt_descubra" class="clear">confira seu resultado</div>
            </div>
            
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>