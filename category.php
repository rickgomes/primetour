<?php get_header(); ?>

<?php $pegar_categoria = get_category($cat); ?>

<!--CONTEÚDO-->
<?php $bg_pagina = rwmb_meta('prime_estilo'); ?>

<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
            <div class="titulo">Blog</div>
            
            <div class="menu_personalizado blog contato">
            	<?php $opcoes_menu = array('menu'=> 'blog','container'=> '', 'theme_location'  => 'blog','items_wrap'=> '<ul class="menu">%3$s</ul>',); wp_nav_menu( $opcoes_menu ); ?>
            </div>
            
            <!--BOX BLOG-->
    <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>        
    <?php query_posts(array('posts_per_page'=> '6', 'post_type' => 'post' , 'cat' => $pegar_categoria->term_id , 'paged' => $paged)); ?>
    <?php if (have_posts()): ?> 
            <div id="box_blog">
            	<div id="box_resultados" class="hotel">
                
				<?php while (have_posts()) : the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="box_post">
                        <?php echo the_post_thumbnail('resultado_busca', array( 'class' => 'imagem_post' ) ); ?>
                        <div class="fade_black"></div>
                        <div class="fade_mais"></div>
                        <div class="infos">
                            <span class="nome_cat">
								<?php $nome_categoria = get_the_category($post->ID); ?>
                                <?php if(($nome_categoria[0]->term_id) == 1) { echo $nome_categoria[1]->cat_name; } else { echo $nome_categoria[0]->cat_name; } ?>
                            </span>
                            <span class="titulo_post"><?php the_title(); ?></span>
                        </div>
                    </a>
				<?php endwhile; ?> 
                    
                    <?php echo wp_pagenavi(); ?>
                </div>
            </div>
	<?php endif; ?>
    <?php wp_reset_query(); ?>
            
        </div>
        
    </div>
    
</div>

<?php get_footer(); ?>