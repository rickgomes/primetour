<a href="<?php the_permalink(); ?>" class="box_post">
	<?php global $post; 
		  $term_id = get_the_terms($post->id, 'destinos');
		  $busca_filha = get_term_children($term_id[0]->term_id, 'destinos');
		  $term_id = get_the_terms($busca_filha[0]->term_id, 'destinos');
	?>	  
	
	<?php echo the_post_thumbnail('resultado_busca', array( 'class' => 'imagem_post' ) ); ?>
    <div class="fade_black"></div>
    <div class="fade_mais"></div>
    <div class="infos">
    
    	<span class="nome_cat"><?php echo rwmb_meta('prime_titulo_hotel'); ?></span>
		<span class="titulo_post"><?php the_title(); ?></span>
        <p class="descricao"><?php echo the_excerpt_max_charlength(70); ?></p>
    </div>
</a>
