<?php

//ADICIONANDO METABOXES
include (TEMPLATEPATH . '/custom/metaboxes.php');

//ADICIONANDO METABOXES
include (TEMPLATEPATH . '/plugins/save_formato.php');

//ADICIONANDO TAXONOMIA DESTINOS, MÊS, FERIADO, ESTILO E INFINITE
include (TEMPLATEPATH . '/custom/local.php');
include (TEMPLATEPATH . '/custom/mes.php');
include (TEMPLATEPATH . '/custom/feriado.php');
include (TEMPLATEPATH . '/custom/estilo.php');
include (TEMPLATEPATH . '/custom/tags_hotel.php');
include (TEMPLATEPATH . '/custom/tags_infinite.php');
include (TEMPLATEPATH . '/custom/infinite_continente.php');
include (TEMPLATEPATH . '/custom/agencias_infinite.php');

//ADICIONANDO POSTS DESTINOS
define("prime_destino_post_type", 'destino');
include (TEMPLATEPATH . '/custom/destino.php');

//ADICIONANDO POSTS HOTEIS
define("prime_hotel_post_type", 'hotel');
include (TEMPLATEPATH . '/custom/hoteis.php');

//ADICIONANDO POSTS BANNERS
define("prime_banner_post_type", 'banner');
include (TEMPLATEPATH . '/custom/banners.php');

//ADICIONANDO POSTS EMPRESAS
define("prime_empresa_post_type", 'empresa');
include (TEMPLATEPATH . '/custom/empresas.php');

//ADICIONANDO CATEGORIA PARA OS BANNERS
include (TEMPLATEPATH . '/custom/cat_banners.php');

//ADICIONANDO FORMATAÇÃO
include (TEMPLATEPATH . '/plugins/formatos_css.php');

//ADICIONANDO FORMATAÇÃO
include (TEMPLATEPATH . '/plugins/get_mes.php');

//ADICIONANDO POSTS INFINITE
define("prime_infinite_post_type", 'infinite');
include (TEMPLATEPATH . '/custom/infinite.php');

//ADICIONANDO POSTS INFINITE - ROTEIROS
define("prime_infinite_roteiros_post_type", 'infinite_roteiros');
include (TEMPLATEPATH . '/custom/infinite_roteiros.php');

//ADICIONANDO POSTS INFINITE - TEAM
define("prime_infinite_team_post_type", 'infinite_team');
include (TEMPLATEPATH . '/custom/infinite_team.php');


//RETIRANDO AJUDA NATIVA WORDPRESS
function hide_help() {
    echo '<style type="text/css">
            #contextual-help-link-wrap { display: none !important; }
          </style>';
}
add_action('admin_head', 'hide_help');

//ADICIONANDO MENUS
add_theme_support('menus');
register_nav_menus(
  array(
    'menu' => 'Menu',
    'menu_rodape' => 'Menu Rodapé',
    'submenu_destinos' => 'Sub Menu Destinos',
    'submenu_especiais' => 'Sub Menu Viagens Especiais',
    'menu_contato' => 'Menu Contato',
    'menu_especiais' => 'Menu Especiais',
    'celebrations' => 'Celebrations',
    'blog' => 'Blog',
  ));
  
// ADICIONANDO SUPORTE A THUMBNAILS
add_action('after_setup_theme', 'prime_thumbs');
function prime_thumbs(){
	if (function_exists('add_theme_support')) {
		global $prime_data;
		add_theme_support( 'post-thumbnails' );
		add_image_size('interna_padrao', 1200, 710, true);
		add_image_size('box_prime', 370, 460, true);
		add_image_size('filtro_imagem', 1170, 500, true);
		add_image_size('resultado_busca', 370, 370, true);
		add_image_size('banner_principal', 1920, 1024, true);
		add_image_size('destaque_home', 480, 780, true);
		add_image_size('destaque_home_mobile', 784, 828, true);
		add_image_size('banner_mobile', 640, 960, true);
		add_image_size('agencia', 400, 200, true);
	}
}

//RETIRANDO OPÇÃO DE ESCOLHA DE COR E OPÇÕES PESSOAIS
function admin_color_scheme() {
   global $_wp_admin_css_colors;
   $_wp_admin_css_colors = 0;
}
add_action('admin_head', 'admin_color_scheme');

if ( ! function_exists( 'cor_remove_personal_options' ) ) {
  function cor_remove_personal_options( $subject ) {
    $subject = preg_replace( '#<h3>Opções pessoais</h3>.+?/table>#s', '', $subject, 1 );
    return $subject;
  }

  function cor_profile_subject_start() {
    ob_start( 'cor_remove_personal_options' );
  }

  function cor_profile_subject_end() {
    ob_end_flush();
  }
}
add_action( 'admin_head', 'cor_profile_subject_start' );
add_action( 'admin_footer', 'cor_profile_subject_end' );


//ADICIONANDO ASSINATURA NO BACKEND
function remove_footer_admin () {
  echo 'Desenvolvido por <a href="http://www.baked.ag" target="_blank">Baked</a>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

//REMOVENDO VERSÃO DO WP DO RODAPÉ - BACKEND
function change_footer_version() {
  return 'Versão 1.0';
}
add_filter( 'update_footer', 'change_footer_version', 9999 );

//RETIRAR LOGO DO WP DA BARRA SUPERIOR
function wps_admin_bar() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');
    $wp_admin_bar->remove_menu('about');
    $wp_admin_bar->remove_menu('wporg');
    $wp_admin_bar->remove_menu('documentation');
    $wp_admin_bar->remove_menu('support-forums');
    $wp_admin_bar->remove_menu('feedback');
    $wp_admin_bar->remove_menu('view-site');
}
add_action( 'wp_before_admin_bar_render', 'wps_admin_bar' );

//MOSTRAR BARRA SUPERIOR SOMENTE PARA ADMINSTRADOR
add_filter('show_admin_bar', '__return_false');

//DASHBOARD COM APENAS UMA COLUNA
function single_screen_columns( $columns ) {
    $columns['dashboard'] = 2;
    return $columns;
}
add_filter( 'screen_layout_columns', 'single_screen_columns' );
function single_screen_dashboard(){return 2;}
add_filter( 'get_user_option_screen_layout_dashboard', 'single_screen_dashboard' );

//// quita unos cuantos paneles del dashboard
function quita_dashboard_widgets() {
    // Globalize the metaboxes array, this holds all the widgets for wp-admin
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'quita_dashboard_widgets' );

//ADICIONANDO CSS DO ADMIN
function custom_admin() {
    $url = get_option('siteurl');
    $url = $url . '/wp-content/themes/prime/css/wp_admin.css';
    echo '<!-- CSS CUSTOMIZAÇÃO -->
          <link rel="stylesheet" type="text/css" href="' . $url . '" />';
}
add_action('admin_head', 'custom_admin');
add_action('login_head', 'custom_admin');

// ADICIONAR FAVICON NO BACKEND
function admin_favicon() { 
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_bloginfo('stylesheet_directory').'/images/favicon.png" />'; 
} 
add_action('admin_head', 'admin_favicon');

//Adicionar Mensagem no painel de controle
add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets() {
global $wp_meta_boxes;
wp_add_dashboard_widget('custom_help_widget', 'Bem Vindo', 'custom_dashboard_help');
}
function custom_dashboard_help() {
$usuario = wp_get_current_user();
echo '
<div id="boas_vindas">
	<h2 class="hndle">Olá, '.$usuario->user_firstname.'!</h2>
	<p>Em caso de dúvidas, entre em contato conosco. 
	<b>Tel: 11 3271 3775</b></p>
</div>
';
}

//Limitando Caracteres no Resumo
function the_excerpt_max_charlength($charlength) {
	$excerpt = get_the_excerpt();
	$charlength++;

	if ( mb_strlen( $excerpt ) > $charlength ) {
		$subex = mb_substr( $excerpt, 0, $charlength - 5 );
		$exwords = explode( ' ', $subex );
		$excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
		if ( $excut < 0 ) {
			echo mb_substr( $subex, 0, $excut );
		} else {
			echo $subex ;
		}
		echo '...';
	} else {
		echo $excerpt ;
	}
}

//Adicionar Contadores no Dashboard
add_action( 'dashboard_glance_items', 'cpad_at_glance_content_table_end' );
function cpad_at_glance_content_table_end() {
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object';
    $operator = 'and';

    $post_types = get_post_types( $args, $output, $operator );
	echo '<div id="principais"><h2 class="hndle">Hotéis e Destinos</h2>';
    foreach ( $post_types as $post_type ) {
        $num_posts = wp_count_posts( $post_type->name );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( $post_type->labels->singular_name, $post_type->labels->name, intval( $num_posts->publish ) );
        if ( $post_type->labels->singular_name == 'Banners / Home' ) {
		} else {	
        if ( current_user_can( 'edit_posts' ) ) {
            $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
            echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
        }
        }
    }
	echo '</div>';
	
		
 $taxonomies = get_taxonomies( $args , $output , $operator ); 
 echo '<div id="principais"><h2 class="hndle">Categorias dos Destinos e Hotéis</h2>';
 foreach( $taxonomies as $taxonomy ) {
  $num_terms  = wp_count_terms( $taxonomy->name );
  $num = number_format_i18n( $num_terms );
  $text = _n( $taxonomy->labels->singular_name, $taxonomy->labels->name , intval( $num_terms ));
  if ( $taxonomy->labels->singular_name == 'Local Banners' ) {
  } else {	
  if ( current_user_can( 'manage_categories' ) ) {
	  $output = '<a href="edit-tags.php?taxonomy=' . $taxonomy->name . '">' . $num . ' ' . $text . '</a>';
	  echo '<li class="post-count ' . $taxonomy->name . '-count">' . $output . '</li>';
  }
  }
 }
 echo '</div>';
}

// CRIANDO O CAMINHO DO PÃO
function wp_custom_breadcrumbs() {
 
  $showOnHome = 0; 
  $delimiter = ' / '; 
  $home = 'home';
  $showCurrent = 1;
  $before = '<span class="ativo">';
  $after = '</span>';
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="breadcrumb"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="breadcrumb"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'categoria "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Busca por "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="javascript:history.go(-1)">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      //echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} 

// ALTERANDO ORDEM DE EXIBIÇÃO DAS CATEGORIAS NO ADMINISTRADOR
add_filter( 'get_terms_args', 'wpse_53094_sort_get_terms_args', 10, 2 );
$teste = array ('destinos', 'mes', 'feriado');
function wpse_53094_sort_get_terms_args( $args, $teste ) {
    global $pagenow;
    if( !is_admin() || ('post.php' != $pagenow && 'post-new.php' != $pagenow) ) 
        return $args;

    $args['orderby'] = 'id';
    $args['order'] = 'ASC';

    return $args;
}

//VERIFICA SE É UM HOTEL OU DESTINO
function is_custom_post_type( $post = NULL ){
    $all_custom_post_types = get_post_types( array ( '_builtin' => FALSE ) );
    if ( empty ( $all_custom_post_types ) )
        return FALSE;
    $custom_types      = array_keys( $all_custom_post_types );
    $current_post_type = get_post_type( $post );

    if ( ! $current_post_type )
        return FALSE;
    return in_array( $current_post_type, $custom_types );
}

//CRIA PÁGINAÇÃO DA TAXONOMIAS
$option_posts_per_page = get_option( 'posts_per_page' );
add_action( 'init', 'my_modify_posts_per_page', 0);
function my_modify_posts_per_page() {
    add_filter( 'option_posts_per_page', 'my_option_posts_per_page' );
}
function my_option_posts_per_page( $value ) {
    global $option_posts_per_page;
    if ( is_tax( 'destinos') ) {
        return 2;
    } else {
        return $option_posts_per_page;
    }
}

//EXIBIR PÁGINAÇÃO DAS PUBLICAÇÕES
function tax_cat_active( $output, $args ) {
  if(is_single()){
    global $post;
    $terms = get_the_terms( $post->ID, $args['taxonomy'] );
    foreach( $terms as $term )
        if ( preg_match( '#cat-item-' . $term ->term_id . '#', $output ) )
            $output = str_replace('cat-item-'.$term ->term_id, 'cat-item-'.$term ->term_id . ' ativo', $output);
  }
  return $output;
}
add_filter( 'wp_list_categories', 'tax_cat_active', 10, 2 );

//PEGAR PÁGINA EXATA
function current_page_url() {
	$pageURL = 'http';
	if( isset($_SERVER["HTTPS"]) ) {
		if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	}
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

//Adicionando nome ao select do formulário
function ov3rfly_replace_include_blank($name, $text, &$html) {
        $matches = false;
        preg_match('/<select name="' . $name . '"[^>]*>(.*)<\/select>/iU', $html, $matches);
        if ($matches) {
            $select = str_replace('<option value="">---</option>', '<option value="">' . $text . '</option>', $matches[0]);
            $html = preg_replace('/<select name="' . $name . '"[^>]*>(.*)<\/select>/iU', $select, $html);
        }
    }

function my_wpcf7_form_elements($html) {
    ov3rfly_replace_include_blank('readeInteresse', 'Área de Interesse', $html);
    return $html;
}
add_filter('wpcf7_form_elements', 'my_wpcf7_form_elements');


//Formulário de Self Booking 
function buscar_empresa() {
  global $wpdb;

  if (empty( $_POST['nome'] )) {
      echo json_encode( array( 'resposta'=>false, 'linkempresa'=>__(''), 'mensagem'=>__('Erro. Por favor verifique o preenchimento.')));
	  die();
  }
  
  $query = new WP_Query( array(
	  'post_type' => 'empresa',
	  's'=> $_POST['nome']
  ));

  $posts = $query->get_posts();
  
  if (!empty ($posts)) {
  
	  foreach($posts as $post) {
		   //echo json_encode( array( 'resposta'=>true, 'linkempresa'=>__('<a href="'.get_post_meta($post->ID, 'prime__link_empresa', true).'" target="_blank" class="link_portal">Acessar portal'.$post->post_title.'</a>'), 'mensagem'=>__('')));
		   echo json_encode( array( 'resposta'=>true, 'linkempresa'=>__(''.get_post_meta($post->ID, 'prime__link_empresa', true).''), 'mensagem'=>__('')));
		   die();
	  }
  
  } else {
      echo json_encode( array( 'resposta'=>false, 'linkempresa'=>__(''), 'mensagem'=>__('Erro. Nenhuma empresa encontrada.')));
	  die();
  }
				
}
 
add_action('wp_ajax_buscar_empresa', 'buscar_empresa');
add_action('wp_ajax_nopriv_buscar_empresa', 'buscar_empresa');

// ADICIONANDO SCRIPT PARA REST API
function wpsd_add_book_args() {
    global $wp_post_types;
 
    $wp_post_types['hotel']->show_in_rest = true;
    $wp_post_types['hotel']->rest_base = 'hotel';
    $wp_post_types['hotel']->rest_controller_class = 'WP_REST_Posts_Controller';
}
add_action( 'init', 'wpsd_add_book_args', 30 );

//AJUSTANDO ERRO DA VERSÃO 4.9 de CUSTOM PAGE
function wp_42573_fix_template_caching( WP_Screen $current_screen ) {

  // Only flush the file cache with each request to post list table, edit post screen, or theme editor.
  if ( ! in_array( $current_screen->base, array( 'post', 'edit', 'theme-editor' ), true ) ) {
    return;
  }

  $theme = wp_get_theme();
  if ( ! $theme ) {
    return;
  }

  $cache_hash = md5( $theme->get_theme_root() . '/' . $theme->get_stylesheet() );
  $label = sanitize_key( 'files_' . $cache_hash . '-' . $theme->get( 'Version' ) );
  $transient_key = substr( $label, 0, 29 ) . md5( $label );
  delete_transient( $transient_key );
}
add_action( 'current_screen', 'wp_42573_fix_template_caching' );

// REDIRECIONANDO USUÁRIOS DO INFINITE AO ACESSO WP-ADMIN
$url = get_option('siteurl');
function restrict_admin_with_redirect() {
  if ( !current_user_can( 'edit_pages' ) && ( ! wp_doing_ajax() ) ) {
      wp_redirect(".$url./infinite-destinos"); 
      exit;
  }
}
add_action( 'admin_init', 'restrict_admin_with_redirect', 1 )
?>