<?php get_header(); ?>

<?php 
//OBTENDO DESTINOS
global $post; 
$term_id = get_the_terms($post->id, 'destinos');
$busca_filha = get_term_children($term_id[0]->term_id, 'destinos');
$term_id = get_the_terms($busca_filha[0]->term_id, 'destinos');
?>	 
 
<!--CONTEÚDO-->
<div id="conteudo_geral" class="mapa">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
    	<div class="box_texto">
        
           	<div class="subtitulo"><?php echo $term_id[0]->name; ?></div>
           	<div class="titulo"><?php the_title(); ?></div>
            
            <?php $images = rwmb_meta( 'prime_slider_fotos', 'type=image&size=interna_padrao' );
			if ($images =="") {
			} else {
				echo '<div class="banner_principal">';
				echo '<div id="slider_destino" class="cycle-slideshow"
					data-cycle-fx="fade"
					data-cycle-swipe=true
					data-cycle-pause-on-hover="false"
					data-cycle-speed="400"
					data-cycle-slides="> img">';
            	foreach ( $images as $image ) { echo "<img src='{$image['url']}' alt='{$image['alt']}' /> "; }
				echo '<div id="paginacao_destino" class="cycle-pager"></div></div></div>'; 
			}
			?>
            
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            
            <!--COLUNA ESQUERDA-->
            <div id="infos_destino">
            
            	<?php the_content(); ?>
                
                <?php if ((rwmb_meta('prime_experiencia')) == ""){ ?>
				<?php } else { ?>
                    <span class="titulo_experiencia">Experiences</span>
                    <div id="box_experiencia">
                        <?php echo rwmb_meta('prime_experiencia'); ?>
                    </div>	
				<?php } ?>
                
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="quero_ir">Quero IR!</a>
            </div>
            
            <!--COLUNA DIREITA-->
            <div id="sidebar_destino">
            
				<?php $link_mapa = rwmb_meta('prime_mapa');
                if ($link_mapa == "") {
                } else {
                echo '<div id="tempo">';
                echo do_shortcode ('[awesome-weather location="'.$link_mapa.'" override_title="'.$link_mapa.'" units="C" size="wide" forecast_days="3" hide_stats="0" custom_bg_color="#e9e9e9" inline_style="width: 100%;" background_by_weather="0" text_color="#897f7f" locale="pt_BR"]');
                echo '</div>';
                }
                 ?>
                
                <div id="meses">
                	<span id="titulo_mes">melhores meses para viajar</span>
                <?php 
					$walker = new My_Walker_Category();
					$args = array( 'walker' => $walker ,'hide_empty'=> 0,'current_category'=> 1,'taxonomy'=> 'mes','orderby'=> 'id','order'=> 'ASC','title_li'=>'',);
					wp_list_categories($args);
				?>
                </div>
                <a href="<?php echo site_url('/contato?url='.get_the_title().'&endereco='.get_the_permalink().''); ?>" class="fale_conosco">Fale Conosco</a>
            </div>
            
            <div class="clear"></div>
            
			<?php endwhile; ?>
             <?php endif; ?>
            <!--PEGAR HOTÉIS RELACIONADOS-->
            <?php 
			global $post;
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			if ( empty( $post ) && isset( $GLOBALS['post'] ) )
			$post = $GLOBALS['post'];			
			$exibir_hoteis = get_posts(array(
				'post_type' => 'hotel',
				'posts_per_page'=> -1,
				'orderby'=> 'title', 
				'order' => 'ASC',
				'paged' => $paged,
				'meta_query' => array(
					array(
						'key' => 'selecionar_destinos', 
						'value' => '"' . get_the_ID() . '"', 
						'compare' => 'LIKE'
					)
				)
			));
			?>
            
            <?php if( $exibir_hoteis ):  ?>
                <span class="titulo_hotel_int">Hotéis</span>
                <div id="box_resultados" class="hotel">
            <?php foreach( $exibir_hoteis as $post ): setup_postdata($post); ?>
                    <?php get_template_part('content', 'hotel'); ?>
			<?php endforeach; ?>
			<?php wp_link_pages(); ?>
                </div>
            <?php endif; ?>			
            
            <?php wp_reset_query(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>