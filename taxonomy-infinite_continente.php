<?php include (TEMPLATEPATH . '/plugins/infinite_controle.php'); ?>

<?php get_header(); ?>

<?php
$pegar_taxonomia = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$queried_object = get_queried_object();
$categoria_atual = $queried_object->term_id;
$term_id = $categoria_atual;
$taxonomy_name = $pegar_taxonomia->taxonomy;
$parent = get_term($term_id, $taxonomy_name); 
?>

<div id="conteudo_geral" class="mapa">
            
    <!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo">
    
        <div class="box_texto">
            <div class="titulo"><?php echo $parent->name; ?></div>


            <div id="apresentacao_destinos">
                <?php query_posts(array('posts_per_page'=> '-1', 'orderby' => 'name', 'order' => 'ASC', 'post_type' => 'infinite', 'infinite_continente' => $parent->slug)); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <a href="<?php the_permalink(); ?>" class="destino_box_pq">
                        <?php the_post_thumbnail('filtro_imagem_pais'); ?>
                        <div class="fade_black"></div>
                        <div class="borda"></div>
                        <div class="legenda"><p><?php the_title(); ?></p><div class="linha"/></div></div>
                    </a>
                <?php endwhile; endif; ?>
                <div class="clear"></div>
            </div>



        </div>

    </div>
</div>

<?php get_footer(); ?>