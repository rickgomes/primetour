<?php get_header(); ?>

<?php  $termo_busca = $_GET['s']; ?>
        
<div id="conteudo_geral" class="mapa">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
            
                <div class="box_texto">
                
                    <div class="titulo">deixe os seus instintos <br/>te guiarem</div>
                    
                    <?php global $query_string; ?>
                    <?php if (empty($termo_busca)) {  ?>
                    
                    <div id="refine_busca">refine sua busca</div>
                    
                	<? //Loop dos Hoteis e Destinos ?> 
                    <div id="box_busca">
                    <?php echo do_shortcode( '[searchandfilter taxonomies="destinos,mes,feriado,estilo" types="select" add_search_param=1 hierarchical=1 order_by="id,id,id,id" hide_empty=0,0,0,0 headings="Destino, Mês Ideal, Feriado, Seu Estilo" submit_label="Filtrar"]' ); ?>
                    </div>
                    
                    <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
                    <?php global $query_string; ?>
					<?php query_posts($query_string . '&posts_per_page=3&post_type=hotel&order=ASC'); ?>
					<?php $do_not_duplicate = array(); wp_reset_query();
                    
                    // 1. Loop
                    query_posts($query_string . '&posts_per_page=-1&post_type=hotel&order=ASC');
                    while ( have_posts() ) : the_post();
                        $locations = get_field('selecionar_destinos');
                        $do_not_duplicate[] = $locations[0]->ID; 
                    endwhile;
                    
                    // 2. Loop
                    query_posts($query_string . '&posts_per_page=1&post_type=hotel&order=ASC');
                    while (have_posts()) : the_post();
                        if ( !in_array( $post->ID, $do_not_duplicate ) ) {   
                           $result = array_unique($do_not_duplicate);
                        }
                    endwhile;
					// Verifica se os resultados não estão vazios nas busca por hotéis
                        if (empty($result) ) { ?> 
                                    <div id="resultado_busca" style="display:block; height:30px; margin:100px 0; text-align:center;">
                                        Não foram encontrados resultados.
                                    </div>
                                </div> 
                            </div>
                        </div>        
                        
                        <?php get_footer(); ?>
                        <?php } else { ?> 
						<?php
                        $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        query_posts( array(
                            'posts_per_page' => 3,
                            'post_type' => 'destino',
                            'orderby'=> 'title', 
                            'order' => 'ASC',
                            'paged' => $paged,
                            'post__in'	=> $result
                            ));
                        
                          if (have_posts()) :
                              echo '<div id="box_resultados" class="destino">';
                              echo '<div class="titulo_busca">Resultados para <span>Destinos</span></div>';
                              echo '<div id="box_destino">';
                              
                          while ( have_posts() ) : the_post();
                              get_template_part('content', 'destino');
                          endwhile;
                          
                                echo wp_pagenavi();
                                echo '</div>';
                                echo '</div>';
                           
                          endif; 
                           wp_reset_query();
                    ?>
                    
                    <?php global $query_string; ?>
					<?php query_posts($query_string . '&posts_per_page=3&post_type=hotel&order=ASC'); ?>

                    <?php if (have_posts()) : ?>
                    <div id="box_resultados" class="hotel">
                    <div class="titulo_busca">Resultados para <span>Hotéis</span></div>
                    <div id="box_hotel">
                    
					<?php while(have_posts()) : the_post(); ?>
                    
                            <?php  if ($post->post_type == 'hotel') { ?>
                            	<?php get_template_part('content', 'hotel'); ?>
                            <?php }?>
                        
                    <? endwhile; ?> 
						<? echo wp_pagenavi(); ?>
                        </div>
                        </div>
					<?php endif; ?>
				<? }; ?>                     
                    
                    
                    
					<?php } else { ?>
                    <?php $post=query_posts($query_string . '&posts_per_page=-1post_type=hotel,destino'); ?>
						<?php if (have_posts()) : ?>
                                    <div id="box_resultados" class="hotel">
                                    <div class="titulo_busca">Resultados para a busca por <span><?php echo the_search_query(); ?></span></div>
                                    <div id="box_hotel">
                        
                        <?php while(have_posts()) : the_post(); ?>
                        
                                <?php  if ($post->post_type == 'destino') { ?>
                                    <?php get_template_part('content', 'destino'); ?>
                                <?php }?>
                        
                                <?php  if ($post->post_type == 'hotel') { ?>
                                    <?php get_template_part('content', 'hotel'); ?>
                                <?php }?>
                            
                        <? endwhile; ?> 
                            <? echo wp_pagenavi(); ?>
                            </div>
                            </div>
                        <?php endif; ?>
                    
                    
            <? } ?>
        </div> 
    </div>
</div>        

<?php get_footer(); ?>