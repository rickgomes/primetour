<?php include (TEMPLATEPATH . '/plugins/infinite_controle.php'); ?>

<?php get_header(); ?>

<!--CONTEÚDO-->
<div id="conteudo_geral">
	
	<!--BREADCRUMB-->
    <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
    
    <!--CONTEÚDO DA PÁGINA-->
    <div id="conteudo" class="team">
    
    	<div class="box_texto">

            <div id="box_resumo">
                <div id="box_imagem">
                    <?php echo the_post_thumbnail('resultado_busca', array( 'class' => 'imagem_post' ) ); ?>
                </div>

                <div id="resumo">
                    <div id="logo_agencia">
                        <img src="<?php the_field('logo_agencia'); ?>">
                    </div>
                    <span class="nome"><?php the_title(); ?></span>
                    <span class="regiao"><?php the_field('regiao'); ?></span>
                    <span class="especialista">
                        <strong>Especialista em:</strong>
                        <?php the_field('especialista'); ?>
                    </span>
                </div>

                <div class="clear"></div>
            </div>

            
            <div id="conteudo">
                <span class="titulo_sessao">"<?php the_field('frase_team'); ?>"</span>
                <?php the_content();?>
            </div>

            <div id="bio">
                <span class="titulo_sessao">Fale Comigo</span>
                <div class="info">
                    <div class="box">
                        <img src="<?php bloginfo('template_url'); ?>/images/team_email.jpg">
                    </div>
                    <p><a href="mailto:<?php the_field('email'); ?>" target="_blank">
                    <?php the_field('email'); ?>
                    </a></p>
                </div>
                <div class="info">
                    <div class="box">
                        <img src="<?php bloginfo('template_url'); ?>/images/team_telefone.jpg">
                    </div>
                    <p><?php the_field('telefone'); ?></p>
                </div>
                <div class="info">
                    <div class="box">
                        <img src="<?php bloginfo('template_url'); ?>/images/team_instagram.jpg">
                    </div>
                    <p><a href="https://www.instagram.com/<?php the_field('instagram'); ?>" target="_blank">
                    @<?php the_field('instagram'); ?>
                    </a></p>
                </div>
                <div class="info">
                    <div class="box">
                        <img src="<?php bloginfo('template_url'); ?>/images/team_site.jpg">
                    </div>
                    <p><a href="https://<?php the_field('site'); ?>" target="_blank">
                    <?php the_field('site'); ?>
                    </a></p>
                </div>
            </div>


            <div class="box_cta_infinite">
                <a href="http://primetour.com.br/infinite-contato/" class="ow-icon-placement-left ow-button-hover" id="bt_contato">
                    <span>Seja um IC</span>
                </a>
            </div>

        </div>

    </div>
</div>
<?php get_footer(); ?>