//UTILIZANDO O SKEL (Responsivo)
(function($) {
  var template_css = 'http://primetour.com.br/wp-content/themes/prime/css/';	  
skel.init({
  reset: 'full',
  breakpoints: {
  'global':		{ range: '*', href: template_css + 'css.css', containers: 1400, grid: { gutters: 40 }, viewport: { scalable: false } },
  'tablet':		{ range: '-1170', href: template_css + 'tablet.css', containers: '100%', grid: { gutters: 20 } },
  'mobile':		{ range: '-736', href: template_css + 'mobile.css', containers: '100%!', grid: { collapse: true } }
  },
  plugins: {
	  layers: {
		  config: {
			  speed:1000,
			  mode: 'transform'
		  },
		  menu_mobile: {
			  side:'top',
			  hidden:true,
			  width:'100%',
			  height:'100%',
			  maxHeight:600,
			  position:'top',
			  clickToHide:false,
			  breakpoints:'tablet',
			  animation:'overlayY',
			  orientation:'vertical',
			  html:'<div data-action="moveElement" data-args="sidebar"></div>'
		  },
		  faixa_mobile_bradesco: {
			width:'100%',
			maxHeight:150,
			position:'top-left',
			breakpoints:'tablet',
			html:'<div data-action="moveElement" data-args="lg_mobile"></div><div data-action="moveElement" data-args="barra_lateral"></div>'
		  },
	  }
  }
});
})(jQuery);

// MENU SUPERIOR
$(function(){
	
	//MENU VIAGENS ESPECIAIS
	$("ul.menu_superior li.viagens-especiais").click(function(){
		$("ul.menu_superior li").removeClass("ativo");
		$(this).addClass("ativo");
		$('#sub-menu #sub-destinos').css('display', 'none');
		$('#sub-menu #sub-especiais').css('display','table');
    });
	
	//MENU DESTINOS
	$("ul.menu_superior li.destinos").click(function(){
		$("ul.menu_superior li").removeClass("ativo");
		$(this).addClass("ativo");
		$('#sub-menu #sub-especiais').css('display', 'none');
		$('#sub-menu #sub-destinos').css('display','table');
    });
	
	//REGRA PARA SUB-MENUS
	$("#sub-menu ul").hover(function(){
		$(this).css('display','table');
	},
	function(){
        $(this).css('display', 'none');
        $("ul.menu_superior li").removeClass("ativo");
    });
	
	//SCROOL TO TOP
	$('#barra_lateral .topo').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 2000);
    return false;
	});
	
});

//VERIFICAR CHECKBOX 
$(document).ready(function(){
	var check;
	$("input#check_news").on("click", function(){
		check = $("input#check_news").prop("checked");
		if(check) {
			 $('#info_news').addClass('ativo');
		} else {
			 $('#info_news').removeClass('ativo');
		}
	}); 
	$("input#curriculum").change(function() { $(".inserir_cv span#texto_cv").text(($(this).val())); });
});

//SCRIPT DA PÁGINA DESCUBRA
$(document).ready(function(){
	var UltimaEscolhida
    $('.cx_descubra').click(function() {
		UltimaEscolhida = $('.cx_descubra.selecionado')[0];
	if ($(this).hasClass('selecionado')) {
		$(this).removeClass('selecionado');
	} else {
		if ($('.cx_descubra.selecionado').length == 3) {
			alert ('Você só pode escolher 3 imagens');
			$(this).removeClass('selecionado');
		} else {
			if ($(this).hasClass('selecionado')) {
				$(this).removeClass('selecionado');
			}
			else {  
				$(this).addClass('selecionado');
				
			}
		}
		
		}
	});  
});

//BOTÃO DE RESULTADO DA PÁGINA DESCUBRA
$(document).ready(function() {
	function getBaseUrl() {
    var re = new RegExp(/^.*\//);
    return re.exec(window.location.href);
}
$("#bt_descubra").click(function () {
	if ($('.cx_descubra.selecionado').length == 3) {
	var arr = [];
	$(this).html('Aguarde, estamos pesquisando os melhores destinos para você!')
	$(".cx_descubra.selecionado .selecao").each(function(index, elem){
		arr.push("&estilo" +index+ "=" + $(this).text());
	});
	arr.join("");
	
	$("#resultado_descubra").text(arr.join(""));
	var link_final = $("#resultado_descubra").text();
	window.location.replace("resultado/?" + link_final);
	
	} else {
	  alert ('É necessário selecionar ao menos 3 imagens');
	}
	});
});

//SLIDER NA PÁGINA PRIMETOUR
$(document).ready(function() {
    $('#slider_clientes_prime .panel-row-style').cycle({
		fx: 'carousel',
		slides: 'div.panel-grid-cell',
		carouselVisible: 4,
		carouselFluid : true
	});
});

function validarCPF( cpf ){
	var filtro = /^\d{3}.\d{3}.\d{3}-\d{2}$/i;
	
	if(!filtro.test(cpf))
	{
		window.alert("CPF inválido. Tente novamente.");
		return false;
	}
   
	cpf = remove(cpf, ".");
	cpf = remove(cpf, "-");
	
	if(cpf.length != 11 || cpf == "00000000000" || cpf == "11111111111" ||
		cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" ||
		cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" ||
		cpf == "88888888888" || cpf == "99999999999")
	{
		window.alert("CPF inválido. Tente novamente.");
		return false;
   }

	soma = 0;
	for(i = 0; i < 9; i++)
	{
		soma += parseInt(cpf.charAt(i)) * (10 - i);
	}
	
	resto = 11 - (soma % 11);
	if(resto == 10 || resto == 11)
	{
		resto = 0;
	}
	if(resto != parseInt(cpf.charAt(9))){
		window.alert("CPF inválido. Tente novamente.");
		return false;
	}
	
	soma = 0;
	for(i = 0; i < 10; i ++)
	{
		soma += parseInt(cpf.charAt(i)) * (11 - i);
	}
	resto = 11 - (soma % 11);
	if(resto == 10 || resto == 11)
	{
		resto = 0;
	}
	
	if(resto != parseInt(cpf.charAt(10))){
		window.alert("CPF inválido. Tente novamente.");
		return false;
	}
	
	return true;
 }
 
function remove(str, sub) {
	i = str.indexOf(sub);
	r = "";
	if (i == -1) return str;
	{
		r += str.substring(0,i) + remove(str.substring(i + sub.length), sub);
	}
	
	return r;
}

/**
   * MASCARA ( mascara(o,f) e execmascara() ) CRIADAS POR ELCIO LUIZ
   * elcio.com.br
   */
function mascara(o,f){
	v_obj=o
	v_fun=f
	setTimeout("execmascara()",1)
}

function execmascara(){
	v_obj.value=v_fun(v_obj.value)
}

function cpf_mask(v){
	v=v.replace(/\D/g,"")                 //Remove tudo o que não é dígito
	v=v.replace(/(\d{3})(\d)/,"$1.$2")    //Coloca ponto entre o terceiro e o quarto dígitos
	v=v.replace(/(\d{3})(\d)/,"$1.$2")    //Coloca ponto entre o setimo e o oitava dígitos
	v=v.replace(/(\d{3})(\d)/,"$1-$2")   //Coloca ponto entre o decimoprimeiro e o decimosegundo dígitos
	return v
}

$(document).ready(function() {
	setTimeout(function(){
		$('.date').mask('00/00/0000');
		$('.time').mask('00:00:00');
		$('.date_time').mask('00/00/0000 00:00:00');
		$('.cep').mask('00000-000');
		$('.telefone').mask('(00) 0000-0000');
		$('.celular').mask('(00) 0.0000-0000');
		$('.cpf').mask('000.000.000-00', {reverse: true});
		$('.cnpj').mask('00.000.000/0000-00', {reverse: true});		
	}, 1000);
});   

$(document).ready(function() {
	$("#cpf").blur(function() {
		var value = $(this).val();
		validarCPF(value);
	});

	$("#email").parent("span").append("<span role='alert' class='wpcf7-not-valid-tip validate-email'></span>");

	var email = $("#email"); 
	    email.blur(function() { 
	    	console.log("ok");
	        $.ajax({ 
	            url: '../../verificaEmail.php', 
	            type: 'POST', 
	            data:{"email" : email.val()}, 
	            success: function(data) { 
	            data = $.parseJSON(data);
	            $(".validate-email").html(data.email);
	        } 
	    }); 
	}); 

	    setInterval(function(){
			if($(".validate-email").text() == "Ja existe um usuario cadastrado com este email"){
				$("input.wpcf7-submit").attr('disabled','disabled').css('cursor', 'no-drop');
			} else{
				$("input.wpcf7-submit").removeAttr('disabled').css('cursor', 'pointer');
			}
	    	
	    }, 1000);

	
});


$(document).ready(function() {
  $('#bt_busca_empresa').click( function(event) {
		$('form#busca_selfbooking #error').hide();
		$('form#busca_selfbooking p.status').css('display', 'inline-block').text('Buscando Empresa.');
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url:  'http://primetour.com.br/wp-admin/admin-ajax.php',
			data: { 
				'action': 'buscar_empresa',
				'nome': $('form#busca_selfbooking #busca_empresa').val(), 
				},
			success: function(data){
				if (data.resposta == true){
					$('form#busca_selfbooking p.status').hide();
					//$('form#busca_selfbooking #resultado').html(data.linkempresa);
					//document.location.href = data.linkempresa;
					window.open(data.linkempresa, '_blank');
				} else {
					$('form#busca_selfbooking p.status').hide();
					$('form#busca_selfbooking #error').text(data.mensagem).css('display', 'inline-block');
				}
			}
		});
		event.preventDefault();
	});
});


$(document).ready(function() {
 	$("<a href='#' class='link_bradesco'><div class='fade_black'></div><div class='fade_mais'></div></a>").insertAfter(".bradesco_boxes .panel-grid-cell .panel-last-child");
    $(".bradesco_boxes .panel-grid-cell .fade_mais").hover(function(e) {
        e.preventDefault();
        var j = $(this).closest(".panel-grid-cell").find(".sow-image-container a").attr('href');
        $(this).closest(".panel-grid-cell").find(".link_bradesco").attr("href", j);
        $(this).closest(".panel-grid-cell").find(".box_info_bradesco").addClass("ativo");
    },function(){
	    $(this).closest(".panel-grid-cell").find(".box_info_bradesco").removeClass("ativo");
	});

	$(".bradesco_boxes .panel-grid-cell .fade_mais").trigger('mouseover').stop();
	$(".box_info_bradesco").removeClass("ativo");
});


