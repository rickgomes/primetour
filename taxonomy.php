<?php get_header(); ?>


        <?php
		$pegar_taxonomia = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
        $queried_object = get_queried_object();
		$categoria_atual = $queried_object->term_id;
		$term_id = $categoria_atual;
		$taxonomy_name = $pegar_taxonomia->taxonomy;
		$parent = get_term($term_id, $taxonomy_name); 
		$children = get_term_children($term_id, $taxonomy_name);
		$nome_pai = get_term($parent->parent, $taxonomy_name);
		?>
        
        
        <?php if (($taxonomy_name) == 'destinos') { ?>
        <!-- CATEGORIA 1 - Realiza a exibição dos Paises-->        
		<?php if(($parent->parent == 0 && sizeof($children) > 0)) { ?>
        <div id="conteudo_geral" class="mandala">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
                    
                <div class="box_texto">
                    <div class="titulo"><?php echo $parent->name; ?></div>
                    <div id="apresentacao_destinos">
                        <?php $termchildren = get_term_children( $parent->term_id, $taxonomy_name);
							  $teste = get_terms( $taxonomy_name, array( 'child_of' => $parent->term_id, 'orderby' => 'name') );
								$namearray = array();
                              foreach ( $termchildren as $child ) {
                              $term = get_term_by( 'id', $child, $taxonomy_name );
							  $namearray[$term->slug] = $term;
							  }
							  
							  ksort($namearray);
            				  foreach ($namearray as $key => $value) { ?>
                              
                                <a href=" <?php echo esc_url(get_term_link($value)) ?>" class="destino_box_pq">
                                    <?php echo taxonomy_featured_image($value->term_id, 'filtro_imagem_pais'); ?>
                                    <div class="fade_black"></div>
                                    <div class="borda"></div>
                                    <div class="legenda"><p><?php echo $value->name; ?></p><div class="linha"/></div></div>
                                </a>
                        <?php } ?> 
                        <div class="clear"></div>
                    </div>
                </div>  
                
            </div>
        </div>
        
        <!-- CATEGORIA 2 - Se houver uma terceira categoria filha-->        
		<?php } elseif(($parent->parent > 0) && (sizeof($children) > 0)) { ?>
        
        
        <!-- CATEGORIA 3 - Correto é exibir a lista de hotéis e Destinos-->        
		<?php } elseif(($parent->parent > 0) && (sizeof($children) == "")) { ?>
        
        <div id="conteudo_geral" class="mapa">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
            
                <div class="box_texto">
                    <div class="subtitulo"><?php echo $nome_pai->name; ?></div>
                    <div class="titulo"><?php echo $parent->name; ?></div>
                    
                    <div id="refine_busca">refine sua busca</div>
                
                
                <? //Loop dos Hoteis e Destinos ?> 
                    <div id="box_busca">
                    <?php echo do_shortcode( '[searchandfilter taxonomies="destinos,mes,feriado,estilo" types="select" add_search_param=1 hierarchical=1 order_by="id,id,id,id" hide_empty=0,0,0,0 headings="Destino, Mês Ideal, Feriado, Seu Estilo" submit_label="Filtrar"]' ); ?>
                    </div>
                <?php if( have_posts() ){
					$types = array('destino', 'hotel');
					foreach( $types as $type ){
						$args= query_posts( array_merge( array('posts_per_page' => 3, 'taxonomy' => $taxonomy_name, $taxonomy_name => $parent->slug, 'post_type' => $type, 'paged' => $paged, 'orderby'=> 'title', 'order' => 'ASC')));		
						if (empty($args)) {
						} else {
						echo '<div id="box_resultados" class="'.$type.'">';
						$obj = get_post_type_object($type);
						echo '<div class="titulo_busca">Resultados para <span>'.$obj->labels->name.'</span></div>';
						echo '<div id="box_'.$type.'">';
						while( have_posts($args) ){
							
							the_post();
							if( $type == get_post_type() ){ 
								get_template_part('content', $type);
							}
						}
						echo wp_pagenavi();
						echo '</div>';
						echo '</div>';
						rewind_posts();
					}}
				}				
				?>
                </div> 
            </div>
        </div>
        
		<?php } ?>
        
        <?php } elseif (($taxonomy_name) == 'estilo') { ?>
        
        <div id="conteudo_geral" class="mapa">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
            
                <div class="box_texto">
                    <div class="subtitulo"><?php echo $nome_pai->name; ?></div>
                    <div class="titulo"><?php echo $parent->name; ?></div>
                    
                    <div id="refine_busca">refine sua busca</div>
                
                
                <? //Loop dos Hoteis e Destinos ?> 
                    <div id="box_busca">
                    <?php echo do_shortcode( '[searchandfilter taxonomies="destinos,mes,feriado,estilo" types="select" add_search_param=1 hierarchical=1 order_by="id,id,id,id" hide_empty=0,0,0,0 headings="Destino, Mês Ideal, Feriado, Seu Estilo" submit_label="Filtrar"]' ); ?>
                    </div>
                    
                    
<?php 
							
$my_query = new WP_Query(array('posts_per_page' => 3, 'taxonomy' => $taxonomy_name, $taxonomy_name => $parent->slug, 'post_type' => 'hotel', 'paged' => $paged, 'orderby'=> 'title', 'order' => 'ASC'));

$do_not_duplicate = array(); // set befor loop variable as array

// 1. Loop
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
query_posts(array('posts_per_page' => -1, 'taxonomy' => $taxonomy_name, $taxonomy_name => $parent->slug, 'post_type' => 'hotel', 'paged' => $paged, 'orderby'=> 'title', 'order' => 'ASC'));
while ( have_posts() ) : the_post();
	$locations = get_field('selecionar_destinos');
    $do_not_duplicate[] = $locations[0]->ID; // remember ID's in loop
endwhile;

// 2. Loop
query_posts(array('posts_per_page' => 1, 'taxonomy' => $taxonomy_name, $taxonomy_name => $parent->slug, 'post_type' => 'hotel', 'paged' => $paged, 'orderby'=> 'title', 'order' => 'ASC'));
while (have_posts()) : the_post();
    if ( !in_array( $post->ID, $do_not_duplicate ) ) { // check IDs         
	   $result = array_unique($do_not_duplicate);
		//var_dump ($result);
    }
endwhile;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	query_posts( array(
		'posts_per_page' => 3,
		'post_type' => 'destino',
		'orderby'=> 'title', 
		'order' => 'ASC',
		'paged' => $paged,
		'post__in'	=> $result
		));
	
	  if (have_posts()) :
		  echo '<div id="box_resultados" class="destino">';
		  echo '<div class="titulo_busca">Resultados para <span>Destinos</span></div>';
		  echo '<div id="box_destino">';
		  
	  while ( have_posts() ) : the_post();
		  get_template_part('content', 'destino');
	  endwhile;
	  
			echo wp_pagenavi();
			echo '</div>';
			echo '</div>';
	   
	  endif; 
	   wp_reset_query();
?>
				
				

				<?php 
                // PEGAR HOTÉIS
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $args =query_posts(array_merge(
                      array('posts_per_page' => 3, 
                            'taxonomy' => $taxonomy_name,
                            $taxonomy_name => $parent->slug,
                            'post_type' => 'hotel',
                            'paged' => $paged,
                            'orderby'=> 'title',
                            'order' => 'ASC'
                        )));
                        
                        if( have_posts() ){
                            
                                echo '<div id="box_resultados" class="hotel">';
                                echo '<div class="titulo_busca">Resultados para <span>Hotéis</span></div>';
                                echo '<div id="box_destino">';
                            while( have_posts() ){
                                the_post();
                                
                                    get_template_part('content', 'hotel');
                                wp_reset_postdata();
                            }
                                echo wp_pagenavi();
                                echo '</div>';
                                echo '</div>';
                        }		
						wp_reset_query();		
                ?>
                </div> 
            </div>
        </div>
            
            
        
		<?php } else { ?>
        
        <div id="conteudo_geral" class="mapa">
            
            <!--BREADCRUMB-->
            <div id="fx_breadcrumb"><?php wp_custom_breadcrumbs(); ?></div>
            
            <!--CONTEÚDO DA PÁGINA-->
            <div id="conteudo">
            
                <div class="box_texto">
                    <div class="subtitulo"><?php echo $nome_pai->name; ?></div>
                    <div class="titulo"><?php echo $parent->name; ?></div>
                    
                    <div id="refine_busca">refine sua busca</div>
                
                
                <? //Loop dos Hoteis e Destinos ?> 
                    <div id="box_busca">
                    <?php echo do_shortcode( '[searchandfilter taxonomies="destinos,mes,feriado,estilo" types="select" add_search_param=1 hierarchical=1 order_by="id,id,id,id" hide_empty=0,0,0,0 headings="Destino, Mês Ideal, Feriado, Seu Estilo" submit_label="Filtrar"]' ); ?>
                    </div>
                <?php if( have_posts() ){
					$types = array('destino', 'hotel');
					foreach( $types as $type ){
						$args= query_posts( array_merge( array('posts_per_page' => 3, 'taxonomy' => $taxonomy_name, $taxonomy_name => $parent->slug, 'post_type' => $type, 'paged' => $paged, 'orderby'=> 'title', 'order' => 'ASC')));
						if (empty($args)) {
						} else {
						echo '<div id="box_resultados" class="'.$type.'">';
						$obj = get_post_type_object($type);
						echo '<div class="titulo_busca">Resultados para <span>'.$obj->labels->name.'</span></div>';
						echo '<div id="box_'.$type.'">';
						while( have_posts($args) ){
							
							the_post();
							if( $type == get_post_type() ){ 
								get_template_part('content', $type);
							}
						}
						echo wp_pagenavi();
						echo '</div>';
						echo '</div>';
						rewind_posts();
					}}
				}				
				?>
                </div> 
            </div>
        </div>
            
            
        <?php } ?>
        

<?php get_footer(); ?>