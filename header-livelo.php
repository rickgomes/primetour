<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="Author" content="Agência Baked | baked.ag">

<title><?php wp_title( '|', true, 'right' ); ?> PRIMETOUR</title>

<!--FAVICON-->
<link rel="icon" type="image/ico" href="<?php bloginfo('template_url'); ?>/images/favicon.ico"/>

<!--CSS-->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<!--JAVASCRIPT-->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery_ui.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.mask.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/skel.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/skel_menu.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/banners.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
<?php wp_head(); ?>

<!-- Hotjar Tracking Code for www.primetour.com.br -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:681747,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>

</head>

<body <?php body_class( $class ); ?>>
<!--GOOGLE ANALYTICS-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-76582201-1', 'auto');
  ga('send', 'pageview');
</script>

<!--MENU MOBILE-->
<?php get_sidebar(); ?> 


<!--GERAL-->
<div class="geral">

<?php if ( is_home() || is_404() ) { ?>
<?php } else { ?>
	

	<!--CABEÇALHO-->
    <div id="cabecalho">
        <!--MENU-->
    	<nav id="menu" style="min-height: 91px;">
        	<a href="<?php echo site_url('livelo'); ?>" id="logo"></a>
        </nav>
        <nav id="sub-menu"></nav>
    </div>
<?php } ?>
    
    <!--BARRA LATERAL
    <ul id="barra_lateral">
    	<li class="telefone"><a href="tel:(11) 3178-4760">(11) 3178-4760</a></li>
    	<li class="email"><a href="mailto:primetour@primetour.com.br">primetour@primetour.com.br</a></li>
    	<li class="busca">
        	<div id="barra_busca">
            	<form action="<?php //echo site_url(); ?>" method="get">
                    <input type="text" name="s" id="campo_busca" placeholder="Busca" value="<?php //the_search_query(); ?>" />
                </form>
            </div>
        </li>
    	<li class="topo"><a> Voltar ao Topo</a></li>
    </ul>-->