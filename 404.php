<?php get_header(); ?>

<!--ERRO 404-->
<div id="erro_404" class="geral">
	<a href="<?php echo site_url(); ?>" id="logo"></a>
    
    <div id="conteudo_erro">
    	<span>erro 404</span>
		<p>página não encontrada</p>
        
        <div id="mensagem">
            <p>PARECE QUE VOCÊ SE PERDEU.</p>
            <hr/>
        </div>
        
        <a href="<?php echo site_url();?>" id="bt_retorno">encontre seu destino</a>
    </div>
    
</div>

<?php get_footer(); ?>