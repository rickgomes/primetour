<?php
//Criando as Ações
add_action('init', 'prime_destino_post_type');  

//Registra o Custom Post Type
function prime_destino_post_type() {
	
	//Cria as labels de exibição do destino
	$labels = array(
		    'name' => _x('Destinos', 'destino'),
		    'singular_name' => _x('Destino', 'destino'),
		    'add_new' => _x('Novo Destino', 'Destino'),
		    'add_new_item' => __('Adicionar Novo Destino'),
		    'edit_item' => __('Editar Destino'),
		    'new_item' => __('Novo Destino Adicionado'),
		    'view_item' => __('Ver Destino'),
		    'search_items' => __('Buscar Destino'),
		    'not_found' =>  __('Nenhum Destino foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Destino foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_destino_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'destino'),
			 'taxonomies' => array('destinos','mes','feriado'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-palmtree',
	         'supports' => array('title','thumbnail','editor') ) );
}

?>