<?php
//Adicionar Taxonomia Mês
add_action('init', 'prime_mes'); 
//Registra a Taxonomia criada
function prime_mes(){
	
	register_taxonomy("mes",
	array(prime_mes_post_type),
	array(	"hierarchical" => true,
			"label" => "Mês", 
			"singular_label" => "Mês", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'mes'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_mes( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['mes'] ) && is_numeric( $qv['mes'] ) ) {
      $term = get_term_by( 'id', $qv['mes'], 'mes' );
      $qv['mes'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_mes');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_mes' );
function my_filter_list_mes() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'destino') {
        wp_dropdown_categories( array(
            'show_option_all' => 'Mês',
            'taxonomy' => 'mes',
            'name' => 'mes',
            'orderby' => 'ID',
            'selected' => ( isset( $wp_query->query['mes'] ) ? $wp_query->query['mes'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
    if (($screen->post_type) == 'hotel' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Mês',
            'taxonomy' => 'mes',
            'name' => 'mes',
            'orderby' => 'ID',
            'selected' => ( isset( $wp_query->query['mes'] ) ? $wp_query->query['mes'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>