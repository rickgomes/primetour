<?php
//Adicionar Taxonomia Mês
add_action('init', 'prime_tags_infinite'); 
//Registra a Taxonomia criada
function prime_tags_infinite(){
	

	register_taxonomy("tags_infinite",
	array(prime_tags_infinite_post_type),
	array(	"hierarchical" => true,
			"label" => "Tags", 
			"singular_label" => "Tags", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'tags_infinite'),
	));
}
?>