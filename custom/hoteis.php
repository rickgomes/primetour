<?php
//Criando as Ações
add_action('init', 'prime_hotel_post_type');  

//Registra o Custom Post Type
function prime_hotel_post_type() {
	
	//Cria as labels de exibição do hotel
	$labels = array(
		    'name' => _x('Hotéis', 'hotel'),
		    'singular_name' => _x('Hotel', 'hotel'),
		    'add_new' => _x('Novo Hotel', 'Hotel'),
		    'add_new_item' => __('Adicionar Novo Hotel'),
		    'edit_item' => __('Editar Hotel'),
		    'new_item' => __('Novo Hotel Adicionado'),
		    'view_item' => __('Ver Hotel'),
		    'search_items' => __('Buscar Hotel'),
		    'not_found' =>  __('Nenhum Hotel foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Hotel foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_hotel_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'rewrite' => array('slug'=>'hotel'),
			 'taxonomies' => array('destinos','mes','feriado','estilo','tags_hotel'),
			 'show_in_nav_menus' => true,
			 'show_in_rest' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-admin-multisite',
	         'supports' => array('title','thumbnail','editor') ) );
}

?>