<?php
//Adicionar Taxonomia Estilo de Viagem
add_action('init', 'prime_estilo'); 
//Registra a Taxonomia criada
function prime_estilo(){
	

	register_taxonomy("estilo",
	array(prime_estilo_post_type),
	array(	"hierarchical" => true,
			"label" => "Estilo de Viagem", 
			"singular_label" => "Estilo de Viagem", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'estilo'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_estilo( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['estilo'] ) && is_numeric( $qv['estilo'] ) ) {
      $term = get_term_by( 'id', $qv['estilo'], 'estilo' );
      $qv['estilo'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_estilo');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_estilo' );
function my_filter_list_estilo() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'hotel' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Estilo de Viagem',
            'taxonomy' => 'estilo',
            'name' => 'estilo',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['estilo'] ) ? $wp_query->query['estilo'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}

?>