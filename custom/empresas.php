<?php
//Criando as Ações
add_action('init', 'prime_empresa_post_type');  

//Registra o Custom Post Type
function prime_empresa_post_type() {
	
	//Cria as labels de exibição do empresa
	$labels = array(
		    'name' => _x('Empresas', 'Empresa'),
		    'singular_name' => _x('Empresa', 'Empresa'),
		    'add_new' => _x('Nova Empresa', 'Empresa'),
		    'add_new_item' => __('Adicionar Nova Empresa'),
		    'edit_item' => __('Editar Empresa'),
		    'new_item' => __('Novo empresa Adicionado'),
		    'view_item' => __('Ver Empresa'),
		    'search_items' => __('Buscar Empresa'),
		    'not_found' =>  __('Nenhum Empresa foi encontrada'),
		    'not_found_in_trash' => __('Nenhum empresa foi encontrada na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_empresa_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'empresa'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-admin-multisite',
	         'supports' => array('title') ) );
}

?>