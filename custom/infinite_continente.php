<?php
//Adicionar Taxonomia infinite_continente
add_action('init', 'prime_infinite_continente'); 
//Registra a Taxonomia criada
function prime_infinite_continente(){
	$labels = array(
	  "name"              => __( "Continente" ),
	  "singular_name"     => __( "Continente", "continente" ),
	  "search_items"      => __( "Buscar Continente" ),
	  "all_items"         => __( "Todos os Continente" ),
	  "edit_item"         => __( "Editar Continente" ),
	  "update_item"       => __( "Atualizar Continente" ),
	  "add_new_item"      => __( "Adicionar novo Continente" ),
	  "new_item_name"     => __( "Novo Continente" ),
	  "menu_name"         => __( "Continente" ),
	);

	register_taxonomy("infinite_continente",
	array(prime_infinite_continente_post_type),
	array(	"hierarchical" => true,
			"labels" => $labels,
			"singular_label" => "Local",
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'infinite_continente'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_infinite_continente( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['infinite_continente'] ) && is_numeric( $qv['infinite_continente'] ) ) {
      $term = get_term_by( 'id', $qv['infinite_continente'], 'infinite_continente' );
      $qv['infinite_continente'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_infinite_continente');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_infinite_continente' );
function my_filter_list_infinite_continente() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'infinite' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Continente',
            'taxonomy' => 'infinite_continente',
            'name' => 'infinite_continente',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['infinite_continente'] ) ? $wp_query->query['infinite_continente'] : '' ),
            'hierarchical' => true,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>