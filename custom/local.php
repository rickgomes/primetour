<?php
//Adicionar Taxonomia Destinos
add_action('init', 'prime_destinos'); 
//Registra a Taxonomia criada
function prime_destinos(){
	$labels = array(
	  "name"              => __( "Continente / País" ),
	  "singular_name"     => __( "Local", "local" ),
	  "search_items"      => __( "Buscar Locais" ),
	  "all_items"         => __( "Todos os Locais" ),
	  "parent_item"       => __( "País" ),
	  "parent_item_colon" => __( "Continente" ),
	  "edit_item"         => __( "Editar Local" ),
	  "update_item"       => __( "Atualizar Local" ),
	  "add_new_item"      => __( "Adicionar novo Local" ),
	  "new_item_name"     => __( "Novo Local" ),
	  "menu_name"         => __( "Continente / País" ),
	);

	register_taxonomy("destinos",
	array(prime_destinos_post_type),
	array(	"hierarchical" => true,
			"labels" => $labels,
			"singular_label" => "Local",
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'destinos'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_destinos( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['destinos'] ) && is_numeric( $qv['destinos'] ) ) {
      $term = get_term_by( 'id', $qv['destinos'], 'destinos' );
      $qv['destinos'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_destinos');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_destinos' );
function my_filter_list_destinos() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'destino' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Continente / País',
            'taxonomy' => 'destinos',
            'name' => 'destinos',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['destinos'] ) ? $wp_query->query['destinos'] : '' ),
            'hierarchical' => true,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
    if (($screen->post_type) == 'hotel' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Continente / País',
            'taxonomy' => 'destinos',
            'name' => 'destinos',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['destinos'] ) ? $wp_query->query['destinos'] : '' ),
            'hierarchical' => true,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>