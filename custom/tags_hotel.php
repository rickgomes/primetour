<?php
//Adicionar Taxonomia Mês
add_action('init', 'prime_tags_hotel'); 
//Registra a Taxonomia criada
function prime_tags_hotel(){
	

	register_taxonomy("tags_hotel",
	array(prime_tags_hotel_post_type),
	array(	"hierarchical" => true,
			"label" => "Tags", 
			"singular_label" => "Tags", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'tags_hotel'),
	));
}
?>