<?php
//Criando as Ações
add_action('init', 'prime_banner_post_type');  

//Registra o Custom Post Type
function prime_banner_post_type() {
	
	//Cria as labels de exibição do banner
	$labels = array(
		    'name' => _x('Banners / Home', 'banner'),
		    'singular_name' => _x('Banners / Home', 'banner'),
		    'add_new' => _x('Novo Banner', 'Destino'),
		    'add_new_item' => __('Adicionar Novo Banner'),
		    'edit_item' => __('Editar Banner'),
		    'new_item' => __('Novo Banner Adicionado'),
		    'view_item' => __('Ver Banner'),
		    'search_items' => __('Buscar Banner'),
		    'not_found' =>  __('Nenhum Banner foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Banner foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_banner_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => false,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'banner'),
			 'taxonomies' => array('banners'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-format-gallery',
	         'supports' => array('title','thumbnail') ) );
}

?>