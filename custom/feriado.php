<?php
//Adicionar Taxonomia Feriado
add_action('init', 'prime_feriado'); 
//Registra a Taxonomia criada
function prime_feriado(){
	

	register_taxonomy("feriado",
	array(prime_feriado_post_type),
	array(	"hierarchical" => true,
			"label" => "Feriado", 
			"singular_label" => "Feriado", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'feriado'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_feriado( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['feriado'] ) && is_numeric( $qv['feriado'] ) ) {
      $term = get_term_by( 'id', $qv['feriado'], 'feriado' );
      $qv['feriado'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_feriado');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_feriado' );
function my_filter_list_feriado() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'destino' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Feriado',
            'taxonomy' => 'feriado',
            'name' => 'feriado',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['feriado'] ) ? $wp_query->query['feriado'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
    if (($screen->post_type) == 'hotel' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Feriado',
            'taxonomy' => 'feriado',
            'name' => 'feriado',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['feriado'] ) ? $wp_query->query['feriado'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>