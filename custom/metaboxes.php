<?php
add_filter( 'rwmb_meta_boxes', 'custom_primetour' );
function custom_primetour( $meta_boxes ) {
    $prefix = 'prime_';
	
// Definições para os Banners das Páginas
$meta_boxes[] = array(
	'pages' => array('page'),
	'id' => 'estilo_pagina',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Estilo de Página',

	'fields' => array(
		array(
				'name'        => __( 'Background', 'background' ),
				'id'          => "{$prefix}estilo",
				'type'        => 'select',
				'options'     => array(
					'mapa' => __('Mapa'),
					'mandala' => __('Mandala'),
				),
				'multiple'    => false,
				'placeholder' => __( 'Selecionar Background da Página', 'your-prefix' ),
			),
));

// Definições para Banners dos Destinos e Hoteis
$meta_boxes[] = array(
	'pages' => array('destino','hotel'),
	'id' => 'imagens_destaque',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Slide de Fotos',

	'fields' => array(
		array(
			'id'    => "{$prefix}slider_fotos",
			'desc'  => '',
			'type' => 'image',
			'max_file_uploads' => 4
		),
));

// Definições para Banners dos Destinos e Hoteis
$meta_boxes[] = array(
	'pages' => array('destino','hotel'),
	'id' => 'experiencia_destino',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Experiences',

	'fields' => array(
		array(
			'id'    => "{$prefix}experiencia",
			'desc'  => '',
			'type' => 'wysiwyg',
		),
));

// Definições para Banners dos Destinos e Hoteis
$meta_boxes[] = array(
	'pages' => array('destino','hotel','infinite_roteiros'),
	'id' => 'local_mapa',
	'context' => 'side',
	'priority' => 'low',
	'title' => 'Local Mapa',

	'fields' => array(
		array(
			'id'    => "{$prefix}mapa",
			'desc'  => 'Adicionar cidade e país. <br>Ex: <b>São Paulo, Brazil</b> <br>
			Para buscar locais em casos de duvidas, buscar neste link <a href="http://openweathermap.org/find?q=" target="_blank">Buscar Locais</a>',
			'type' => 'text',
		),
));

// Definições para Hoteis
$meta_boxes[] = array(
	'pages' => array('hotel'),
	'id' => 'informacoes_hotel',
	'context' => 'normal',
	'priority' => 'low',
	'title' => 'Informações do Hotel',

	'fields' => array(
		array(
			'id'    => "{$prefix}titulo_hotel",
			'name'  => 'Digite a Localização Aqui',
			'desc'  => '<b>Informação que aparece acima do nome do hotel</b>',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}endereco",
			'name'  => 'Endereço',
			'desc'  => 'Adicionar o endereço no formato do Google Maps',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}telefones",
			'name'  => 'Telefones',
			'desc'  => 'Adicionar o telefone <b>Ex.: Tel. 1 (808) 874-8000 Fax. 1 (808) 874-2244</b>',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}site",
			'name'  => 'Site',
			'desc'  => 'Adicionar o endereço do site <b style="color:red;">COM HTTP://</b>',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}opinioes",
			'name'  => 'Opiniões de Viajantes',
			'desc'  => 'Adicionar link para site com recomendações. <b style="color:red;">COM HTTP://</b>',
			'type' => 'text',
		),
));

// Definições para Hoteis
$meta_boxes[] = array(
	'pages' => array('hotel'),
	'id' => 'maps_hotel',
	'context' => 'normal',
	'priority' => 'low',
	'title' => 'Opção para Encontrar localização',

	'fields' => array(
		array(
			'id'    => "{$prefix}latitude",
			'name'  => 'Digite a Localização Latitude da localização',
			'desc'  => '<b>Essa informação pode ser obtida através da página do Google Maps</b>',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}longitude",
			'name'  => 'Digite a Localização Longitude da localização',
			'desc'  => '<b>Essa informação pode ser obtida através da página do Google Maps</b>',
			'type' => 'text',
		),
));

// Definições para Banners da Home
$meta_boxes[] = array(
	'pages' => array('banner'),
	'id' => 'banner_mobile',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Imagem no Mobile',

	'fields' => array(
		array(
			'id'    => "{$prefix}_banner_mobile",
			'desc'  => 'Adicionar a imagem que será exibida nas versões mobile do site.<br> Tamanho da imagem deverá ser de 640x960px para o Banner Principal e 784x828px para os destaques.',
			'type' => 'image',
			'max_file_uploads' => 1
		),
));

// Definições para Banners da Home
$meta_boxes[] = array(
	'pages' => array('banner'),
	'id' => 'banner_principal_home',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Caso for um Banner Principal',

	'fields' => array(
		array(
			'id'    => "{$prefix}_titulo_banner_principal",
			'desc'  => 'Adicionar título do Banner Principal',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}_subtitulo_banner_principal",
			'desc'  => 'Adicionar sub-título do Banner Principal',
			'type' => 'text',
		),
));

// Definições para Banners da Home
$meta_boxes[] = array(
	'pages' => array('banner'),
	'id' => 'destaque_home',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Caso for um dos 4 Destaques da Home',

	'fields' => array(
		array(
			'id'    => "{$prefix}_titulo_destaque_homel",
			'desc'  => 'Adicionar título (que ficará em laranja) do Destaque',
			'type' => 'text',
		),
		array(
			'id'    => "{$prefix}_texto_destaque_homel",
			'desc'  => 'Adicionar texto do Destaque',
			'type' => 'text',
		),
));

// Definições para Banners da Home
$meta_boxes[] = array(
	'pages' => array('banner'),
	'id' => 'link_banner',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Link do Banner',

	'fields' => array(
		array(
			'id'    => "{$prefix}_link_banner",
			'desc'  => 'Adicionar o link do para onde o banner irá',
			'type' => 'text',
		),
));

// Definições para Banners da Home
$meta_boxes[] = array(
	'pages' => array('empresa'),
	'id' => 'link_empresa',
	'context' => 'normal',
	'priority' => 'high',
	'title' => 'Link da Página Empresa',

	'fields' => array(
		array(
			'id'    => "{$prefix}_link_empresa",
			'desc'  => 'Adicionar o link personalizdo da empresa.',
			'type' => 'text',
		),
));

	return $meta_boxes;
}
?>