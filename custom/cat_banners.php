<?php
//Adicionar Taxonomia Feriado
add_action('init', 'prime_banner'); 
//Registra a Taxonomia criada
function prime_banner(){
	

	register_taxonomy("banners",
	array(prime_banner_post_type),
	array(	"hierarchical" => true,
			"label" => "Local Banners", 
			"singular_label" => "Local Banner", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'banners'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_banner( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['banner'] ) && is_numeric( $qv['banner'] ) ) {
      $term = get_term_by( 'id', $qv['banner'], 'banner' );
      $qv['banner'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_banner');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_banner' );
function my_filter_list_banner() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'banner' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'Local do Banner',
            'taxonomy' => 'banner',
            'name' => 'banner',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['banner'] ) ? $wp_query->query['banner'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>