<?php
//Criando as Ações
add_action('init', 'prime_infinite_roteiros_post_type');  

//Registra o Custom Post Type
function prime_infinite_roteiros_post_type() {
	
	//Cria as labels de exibição do infinite
	$labels = array(
		    'name' => _x('Infinite Roteiros', 'infinite'),
		    'singular_name' => _x('Infinite', 'infinite'),
		    'add_new' => _x('Novo Roteiro', 'infinite'),
		    'add_new_item' => __('Adicionar Novo Roteiro'),
		    'edit_item' => __('Editar Roteiro'),
		    'new_item' => __('Novo Roteiro Adicionado'),
		    'view_item' => __('Ver Roteiro'),
		    'search_items' => __('Buscar Roteiro'),
		    'not_found' =>  __('Nenhum Roteiro foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Roteiro foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_infinite_roteiros_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => true,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'infinite_roteiros'),
			 'taxonomies' => array('mes'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-admin-site',
	         'supports' => array('title','thumbnail','editor') ) );
}

?>