<?php
//Criando as Ações
add_action('init', 'prime_infinite_team_post_type');  

//Registra o Custom Post Type
function prime_infinite_team_post_type() {
	
	//Cria as labels de exibição do infinite
	$labels = array(
		    'name' => _x('Team', 'team'),
		    'singular_name' => _x('Team', 'team'),
		    'add_new' => _x('Novo Membro', 'team'),
		    'add_new_item' => __('Adicionar Novo Membro'),
		    'edit_item' => __('Editar Membro'),
		    'new_item' => __('Novo Membro Adicionado'),
		    'view_item' => __('Ver Membro'),
		    'search_items' => __('Buscar Membro'),
		    'not_found' =>  __('Nenhum Membro foi encontrado'),
		    'not_found_in_trash' => __('Nenhum Membro foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_infinite_team_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => true,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'infinite_team'),
			 'taxonomies' => array('agencias'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-admin-site',
	         'supports' => array('title','thumbnail','editor') ) );
}

?>