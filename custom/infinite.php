<?php
//Criando as Ações
add_action('init', 'prime_infinite_post_type');  

//Registra o Custom Post Type
function prime_infinite_post_type() {
	
	//Cria as labels de exibição do infinite
	$labels = array(
		    'name' => _x('Infinite', 'infinite'),
		    'singular_name' => _x('Infinite', 'infinite'),
		    'add_new' => _x('Novo País', 'infinite'),
		    'add_new_item' => __('Adicionar Novo País'),
		    'edit_item' => __('Editar País'),
		    'new_item' => __('Novo País Adicionado'),
		    'view_item' => __('Ver País'),
		    'search_items' => __('Buscar País'),
		    'not_found' =>  __('Nenhum País foi encontrado'),
		    'not_found_in_trash' => __('Nenhum País foi encontrado na lixeira'), 
		    'parent_item_colon' => ''
		    );

		    //Registra o Custom Post Type e o que ele vai ter
		    register_post_type( prime_infinite_post_type,
		    array( 
			 'labels' => $labels,
	         'public' => true,  
	         'show_ui' => true,  
	         'capability_type' => 'post',  
	         'hierarchical' => true,  
			 'exclude_from_search' => false,
			 'rewrite' => array('slug'=>'infinite'),
			 'taxonomies' => array('infinite_continente','mes', 'tags_infinite'),
			 'show_in_nav_menus' => true,
			 'menu_position' => 7,
			 'menu_icon' => 'dashicons-admin-site',
	         'supports' => array('title','thumbnail','editor') ) );
}

?>