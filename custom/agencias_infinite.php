<?php
//Adicionar Taxonomia agencias
add_action('init', 'prime_agencias'); 
//Registra a Taxonomia criada
function prime_agencias(){
	

	register_taxonomy("agencias",
	array(prime_agencias_post_type),
	array(	"hierarchical" => true,
			"label" => "Agências", 
			"singular_label" => "Agência", 
			"query_var" => true,
			"show_ui" => true,
			"show_admin_column" => true,
			"rewrite" => array('slug' => 'agencias'),
	));
}

//Filtrar a taxonomia nos posts (dashboard)
function filtrar_agencias( $query ){
   $qv = &$query->query_vars;
   if (isset( $qv['agencias'] ) && is_numeric( $qv['agencias'] ) ) {
      $term = get_term_by( 'id', $qv['agencias'], 'agencias' );
      $qv['agencias'] = $term->slug;
   }
}
add_filter('parse_query','filtrar_agencias');

//Filtrando por Taxonomia
add_action( 'restrict_manage_posts', 'my_filter_list_agencias' );
function my_filter_list_agencias() {
    $screen = get_current_screen();
    global $wp_query;
    if (($screen->post_type) == 'infinite_team' ) {
        wp_dropdown_categories( array(
            'show_option_all' => 'agencias',
            'taxonomy' => 'agencias',
            'name' => 'agencias',
            'orderby' => 'name',
            'selected' => ( isset( $wp_query->query['agencias'] ) ? $wp_query->query['agencias'] : '' ),
            'hierarchical' => false,
            'depth' => 3,
            'show_count' => false,
            'hide_empty' => true,
        ) );
    }
}
?>